docker container stop eshop
docker container rm eshop
docker run -p 8080:8080 -v $PWD:/var/www/myweb/ --name eshop enhuizhu/php7.2-nginx-composer
docker-compose run eshop sh -c 'cd /var/www/myweb/ && composer install'