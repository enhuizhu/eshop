-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Generation Time: Nov 29, 2018 at 08:03 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pic` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL,
  `order` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `pic`, `parent_id`, `order`, `updated_at`, `created_at`, `user_id`) VALUES
(1, 'starter', 'Chinese-Starters.jpg', 0, NULL, '2018-11-29 19:41:46', '2015-09-28 20:14:08', 1),
(3, 'Desert', 'apr13_01_gourmet.jpg', 0, NULL, '2018-11-29 19:41:53', '2015-09-29 19:53:53', 1),
(7, 'Main Course', 'main_course.jpg', 0, NULL, '2018-11-29 19:41:56', '2015-10-11 06:37:40', 1);

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(10) UNSIGNED NOT NULL,
  `tel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `tel`, `address1`, `address2`, `city`, `postcode`, `user_id`, `created_at`, `updated_at`) VALUES
(1, '+447588732089', 'flat 3 rainbow court', 'chipley street', 'london', 'se14 6ez', 1, '2016-10-21 14:47:04', '2016-10-21 14:47:04'),
(2, '+447588732089', 'flat 3 rainbow court', 'chipley street', 'london', 'se14 6ez', 2, '2016-10-28 20:52:33', '2016-10-28 20:52:33');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `shop_user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `username`, `password`, `email`, `created_at`, `updated_at`, `shop_user_id`) VALUES
(1, 'zhuen2000', '$2y$10$gv0ukkL/M10uwoCHndyca.Bq7g8QB0.h5Nkh/fq9FqutJ1bKJJGFO', 'zhuen2000@163.com', '2016-10-09 21:24:12', '2016-10-09 21:24:12', 0),
(2, 'sunlilyy', '$2y$10$h2fRb4KANLrXykFGcdvMUexkprxxad7vkztzV8.DYGVRaVzZd4wyO', 'sunlilyy@gmail.com', '2016-10-09 21:25:59', '2016-10-09 21:25:59', 0);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_08_01_160729_create_users', 1),
('2015_09_25_101551_create_products_table', 1),
('2016_12_04_182412_add_userid_to_cateroy_table', 1),
('2016_12_04_185857_add_shopname_to_user_table', 1),
('2016_12_14_213534_add_user_token_to_usertable', 1),
('2016_12_18_194308_add_logo_and_currency_to_users_table', 1),
('2016_12_18_195614_update_currency_column', 1),
('2016_12_29_210537_add_shop_user_id_to_customer_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `deliver_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `items` text COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `status` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `read` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `contact_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `deliver_time`, `order_time`, `items`, `note`, `status`, `read`, `contact_id`, `user_id`) VALUES
(1, '1477067100', '1477063163', '[{\"id\":\"2\",\"quantity\":2},{\"id\":\"3\",\"quantity\":1},{\"id\":\"6\",\"quantity\":1}]', 'fas', '0', '0', 1, 1),
(2, '1477071900', '1477067678', '[{\"id\":\"3\",\"quantity\":1},{\"id\":\"2\",\"quantity\":1},{\"id\":\"6\",\"quantity\":1}]', 'fsda', '0', '0', 1, 1),
(3, '1477071900', '1477067879', '[{\"id\":\"3\",\"quantity\":1},{\"id\":\"2\",\"quantity\":1},{\"id\":\"6\",\"quantity\":1}]', 'fsda', '0', '0', 1, 1),
(4, '1477071900', '1477163828', '[{\"id\":\"3\",\"quantity\":1},{\"id\":\"2\",\"quantity\":1},{\"id\":\"6\",\"quantity\":1}]', 'fsda', '0', '0', 1, 1),
(5, '1477071900', '1477167843', '[{\"id\":\"3\",\"quantity\":1},{\"id\":\"2\",\"quantity\":1},{\"id\":\"6\",\"quantity\":1}]', 'fsda', '3', '0', 1, 1),
(6, '1477071900', '1477167905', '[{\"id\":\"2\",\"quantity\":1},{\"id\":\"6\",\"quantity\":1}]', 'fsda', '2', '0', 1, 1),
(7, '1477071900', '1477167928', '[{\"id\":\"6\",\"quantity\":1}]', 'fsda', '1', '0', 1, 1),
(8, '1477171800', '1477168017', '[{\"id\":\"6\",\"quantity\":1},{\"id\":\"3\",\"quantity\":2},{\"id\":\"2\",\"quantity\":1}]', 'fsda', '0', '0', 1, 1),
(9, '1477692000', '1477687953', '[{\"id\":\"3\",\"quantity\":3}]', 'fda', '0', '0', 2, 2),
(10, '1477692000', '1477689860', '[{\"id\":\"3\",\"quantity\":3}]', 'fda', '0', '0', 2, 2),
(11, '1477745100', '1477726216', '[{\"id\":\"2\",\"quantity\":1},{\"id\":\"3\",\"quantity\":1},{\"id\":\"6\",\"quantity\":1}]', 'hghgjh', '3', '0', 2, 2),
(12, '1477745100', '1477727405', '[{\"id\":\"2\",\"quantity\":1},{\"id\":\"3\",\"quantity\":1}]', 'hghgjh', '1', '0', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `price` float(8,2) NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `pics` text COLLATE utf8_unicode_ci NOT NULL,
  `feature_pic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `price`, `category_id`, `pics`, `feature_pic`, `created_at`, `updated_at`) VALUES
(2, 'prawn cracks', 'test', 5.00, 1, 'Prawn-crackers-cooked.jpg', NULL, '2015-10-03 16:29:52', '2015-10-03 16:29:52'),
(3, 'spare ribs', 'spare ribs', 10.00, 1, '20110416-pupu-platter-ribs-primary-thumb-625xauto-154500.jpeg', NULL, '2015-10-03 21:28:07', '2015-10-03 21:28:07'),
(6, 'Egg Fried Rice', 'Fantastic Egg Fried Rice in wok Express, Hopefully you enjoy that ', 5.00, 7, 'egg-fried-rice.jpg', NULL, '2015-10-11 06:42:57', '2015-10-11 06:42:57'),
(7, 'Singapore Noodle', 'Singapore Noodle						', 7.50, 7, '1270631403_chinese.jpg', NULL, '2015-10-11 08:15:53', '2015-10-11 08:15:53');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `remember_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shop_name` char(200) COLLATE utf8_unicode_ci NOT NULL,
  `token` char(40) COLLATE utf8_unicode_ci NOT NULL,
  `logo` char(200) COLLATE utf8_unicode_ci NOT NULL,
  `currency` char(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `role`, `active`, `created_at`, `updated_at`, `remember_token`, `shop_name`, `token`, `logo`, `currency`) VALUES
(1, 'davidzhu', 'zhuen2000@163.com', '$2y$10$nGjCFMdCPX.56Hn/WZ3ste7uhbebNydX3l7Ss1tJCSDjtbPa.agHu', 2, 1, '0000-00-00 00:00:00', '2015-10-14 19:28:32', 'IBYGUXO8T7lLoX40YYFENUFyjIPXFAoUH55LIpnYuPo6nLjQYHeryZ8KP2po', '', '', '', ''),
(2, 'admin', 'admin@shop.local', '$2y$10$KsCGp/HFxuXpSKGKbjK2Uef2tFpPrdQa3z8uiHlKPHeEGcXP4dAy6', 2, 1, '0000-00-00 00:00:00', '2015-11-12 19:32:28', '1cLj4TbamOtHQZExyxv2y1plPFF2o4gcy79D7XP2FI26azSPtYa3Y85XGLQE', '', '', '', ''),
(3, 'ella', 'ella@163.com', '$2y$10$swkgpan.1qNiPhVd9BawUeGECnLLyUjJ/HPQBC.HPSksA2n.IsX1W', 2, 1, '2015-09-28 19:45:28', '2015-10-10 19:51:14', 'N3NYakx3nmnNxZOsacmouQCio3yzY8WodSp3CBuieDiBUVGUNLgxtZ18240E', '', '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
