-- MySQL dump 10.13  Distrib 5.5.47, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: eshop
-- ------------------------------------------------------
-- Server version	5.5.47-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pic` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL,
  `order` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'starter','Chinese-Starters.jpg',0,NULL,'2015-09-28 20:14:08','2015-09-28 20:14:08'),(3,'Desert','apr13_01_gourmet.jpg',0,NULL,'2015-10-03 13:04:32','2015-09-29 19:53:53'),(7,'Main Course','main_course.jpg',0,NULL,'2015-10-11 06:37:40','2015-10-11 06:37:40');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` VALUES (1,'+447588732089','flat 3 rainbow court','chipley street','london','se14 6ez',1,'2016-10-21 14:47:04','2016-10-21 14:47:04'),(2,'+447588732089','flat 3 rainbow court','chipley street','london','se14 6ez',2,'2016-10-28 20:52:33','2016-10-28 20:52:33');
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (1,'zhuen2000','$2y$10$gv0ukkL/M10uwoCHndyca.Bq7g8QB0.h5Nkh/fq9FqutJ1bKJJGFO','zhuen2000@163.com','2016-10-09 21:24:12','2016-10-09 21:24:12'),(2,'sunlilyy','$2y$10$h2fRb4KANLrXykFGcdvMUexkprxxad7vkztzV8.DYGVRaVzZd4wyO','sunlilyy@gmail.com','2016-10-09 21:25:59','2016-10-09 21:25:59');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deliver_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `items` text COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `status` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `read` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `contact_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,'1477067100','1477063163','[{\"id\":\"2\",\"quantity\":2},{\"id\":\"3\",\"quantity\":1},{\"id\":\"6\",\"quantity\":1}]','fas','0','0',1,1),(2,'1477071900','1477067678','[{\"id\":\"3\",\"quantity\":1},{\"id\":\"2\",\"quantity\":1},{\"id\":\"6\",\"quantity\":1}]','fsda','0','0',1,1),(3,'1477071900','1477067879','[{\"id\":\"3\",\"quantity\":1},{\"id\":\"2\",\"quantity\":1},{\"id\":\"6\",\"quantity\":1}]','fsda','0','0',1,1),(4,'1477071900','1477163828','[{\"id\":\"3\",\"quantity\":1},{\"id\":\"2\",\"quantity\":1},{\"id\":\"6\",\"quantity\":1}]','fsda','0','0',1,1),(5,'1477071900','1477167843','[{\"id\":\"3\",\"quantity\":1},{\"id\":\"2\",\"quantity\":1},{\"id\":\"6\",\"quantity\":1}]','fsda','3','0',1,1),(6,'1477071900','1477167905','[{\"id\":\"2\",\"quantity\":1},{\"id\":\"6\",\"quantity\":1}]','fsda','2','0',1,1),(7,'1477071900','1477167928','[{\"id\":\"6\",\"quantity\":1}]','fsda','1','0',1,1),(8,'1477171800','1477168017','[{\"id\":\"6\",\"quantity\":1},{\"id\":\"3\",\"quantity\":2},{\"id\":\"2\",\"quantity\":1}]','fsda','0','0',1,1),(9,'1477692000','1477687953','[{\"id\":\"3\",\"quantity\":3}]','fda','0','0',2,2),(10,'1477692000','1477689860','[{\"id\":\"3\",\"quantity\":3}]','fda','0','0',2,2),(11,'1477745100','1477726216','[{\"id\":\"2\",\"quantity\":1},{\"id\":\"3\",\"quantity\":1},{\"id\":\"6\",\"quantity\":1}]','hghgjh','3','0',2,2),(12,'1477745100','1477727405','[{\"id\":\"2\",\"quantity\":1},{\"id\":\"3\",\"quantity\":1}]','hghgjh','1','0',2,2);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `price` float(8,2) NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `pics` text COLLATE utf8_unicode_ci NOT NULL,
  `feature_pic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `products_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (2,'prawn cracks','test',5.00,1,'Prawn-crackers-cooked.jpg',NULL,'2015-10-03 16:29:52','2015-10-03 16:29:52'),(3,'spare ribs','spare ribs',10.00,1,'20110416-pupu-platter-ribs-primary-thumb-625xauto-154500.jpeg',NULL,'2015-10-03 21:28:07','2015-10-03 21:28:07'),(6,'Egg Fried Rice','Fantastic Egg Fried Rice in wok Express, Hopefully you enjoy that ',5.00,7,'egg-fried-rice.jpg',NULL,'2015-10-11 06:42:57','2015-10-11 06:42:57'),(7,'Singapore Noodle','Singapore Noodle						',7.50,7,'1270631403_chinese.jpg',NULL,'2015-10-11 08:15:53','2015-10-11 08:15:53');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `remember_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'davidzhu','zhuen2000@163.com','$2y$10$nGjCFMdCPX.56Hn/WZ3ste7uhbebNydX3l7Ss1tJCSDjtbPa.agHu',1,1,'0000-00-00 00:00:00','2015-10-14 19:28:32','IBYGUXO8T7lLoX40YYFENUFyjIPXFAoUH55LIpnYuPo6nLjQYHeryZ8KP2po'),(2,'admin','admin@shop.local','$2y$10$KsCGp/HFxuXpSKGKbjK2Uef2tFpPrdQa3z8uiHlKPHeEGcXP4dAy6',2,1,'0000-00-00 00:00:00','2015-11-12 19:32:28','1cLj4TbamOtHQZExyxv2y1plPFF2o4gcy79D7XP2FI26azSPtYa3Y85XGLQE'),(3,'ella','ella@163.com','$2y$10$swkgpan.1qNiPhVd9BawUeGECnLLyUjJ/HPQBC.HPSksA2n.IsX1W',2,1,'2015-09-28 19:45:28','2015-10-10 19:51:14','N3NYakx3nmnNxZOsacmouQCio3yzY8WodSp3CBuieDiBUVGUNLgxtZ18240E');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-05 18:57:20
