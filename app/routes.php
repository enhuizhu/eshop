<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

include "routes/adminRoute.php";
/**
* include the route which related to database manipulation
**/
include "routes/databaseRoute.php";
/**
* include the route which relate to api
**/
include "routes/apiRoute.php";




