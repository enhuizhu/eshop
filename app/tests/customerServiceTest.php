<?php

class customerServiceTest extends PHPUnit_Extensions_Database_TestCase {
	public function getConnection() {
		$pdo = new PDO("mysql:host=localhost;dbname=test", 
            "root", "roowpw");
        return $this->createDefaultDBConnection($pdo, 'test');
	}

	public function getDataSet() {
		return $this->createFlatXMLDataSet(dirname(__FILE__).'/_files/seed.xml');
	}

	public function testIsUserExist() {
		$this->assertEquals(customerService::isUserExist("sunlilyy@163.com"), true);
		$this->assertEquals(customerService::isUserExist("dummy@163.com"), false);
	}	
}