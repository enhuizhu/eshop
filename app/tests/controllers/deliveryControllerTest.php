<?php

class DeliveryControllerTest extends TestCase {
  // public function __construct() {
  //   $this->class = App::make('deliveryController');
  // }

  public function setUp()
  {
    $this->class = App::make('deliveryController');
  }
  
  public function testGetDefaultDelivery() {
    $data = new stdClass;
    $data->maxDistance = 6;
    $defaultDelivery = $this->class->getDefaultDelivery($data);

    $this->assertEquals($defaultDelivery->from, 0);
    $this->assertEquals($defaultDelivery->fee, 1);
    $this->assertEquals($defaultDelivery->to, 6);
  }

  public function testCalculateDeliveryFee() {
    $fee1 = new stdClass;
    $fee1->from  = 0;
    $fee1->to = 3;
    $fee1->fee = 1;
    $fee1->priority = 5;
    $info = [$fee1];
    // test the scenario that distance is inside the range
    $result = $this->class->calculateDeliveryFee(0, $info);
    $this->assertEquals($result, 1);
    $result = $this->class->calculateDeliveryFee(1, $info);
    $this->assertEquals($result, 1);
    $result = $this->class->calculateDeliveryFee(3, $info);
    $this->assertEquals($result, 1);
    $result = $this->class->calculateDeliveryFee(4, $info);
    $this->assertEquals($result, false);
  }
}