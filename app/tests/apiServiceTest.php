<?php

class apiServiceTest extends TestCase {
	public function testGenerateToken() {
		$token = apiService::generateToken("enhuizhu", "123456");
		$userInfo = apiService::getUserInfoFromToken($token);

		$this->assertEquals($userInfo->username, "enhuizhu");
		$this->assertEquals($userInfo->password, "123456");
	}

	public function testGetUserInfoFromToken() {
		$userInfo = apiService::getUserInfoFromToken("fdaljflajdlfjaljflakj");
		$this->assertEquals($userInfo, false);
	}

	public function testApiSuccessResponse() {
		$response = apiService::apiSuccessResponse(array("token" => "test"));
		$this->assertEquals($response->success, true);
		$this->assertEquals($response->token, "test");
	}

	public function testIsValidUserToken() {
		$result = apiService::isValidUserToken("eyJpdiI6ImNtdEpLWG12M2VxazhoSHNndEFrK0E9PSIsInZhbHVlIjoiUUxcL2d4Tm90bWt4RFZvZDAxcjVqaTJHVTIwdFl4SFZWNVB6bmR0MUwwWWo1ZXk1bm1QWXhkUituU2hrSjRQcFVTRmNRZWtwN3lraUVsN2FRQnk2S0t5WXNHVmxUOVdBRE9TYmQ3b3drRzUwPSIsIm1hYyI6IjAwMTc1M2E5ZjhhMGQ4YjRlYTRjZDNkY2E4NjQ5OTAwYjNmMDhiMzVlMDRiNmM2OTA4OGNiOGVjNGMzMzM0YzgifQ==");

		$this->assertEquals($result, true);

		$result = apiService::isValidUserToken("eyJpdiI6ImNtdEpLWG12M2VxazhoSHNndEFrK0E9PSIsInZhbHVlIjoiUUxcL2d4Tm90bWt4RFZvZDAxcjVqaTJHVTIwdFl4SFZWNVB6bmR0MUwwWWo1ZXk1bm1QWXhkUituU2hrSjRQcFVTRmNRZWtwN3lraUVsN2FRQnk2S0t5WXNHVmxUOVdBRE9TYmQ3b3drRzUwPSIsIm1hYyI6IjAwMTc1M2E5ZjhhMGQ4YjRlYTRjZDNkY2E4NjQ5OTAwYjNmMDhiMzVlMDRiNmM2OTA4OGNiOGVjNGMz");

		$this->assertEquals($result, false);

		/**
		* expired token
		**/
		$result = apiService::isValidUserToken("eyJpdiI6ImtxYnZpYlRiV0Z4ZXZDTG1hdWZGbEE9PSIsInZhbHVlIjoicUdaXC92Wm5XSmhjV0RabGZJRUw1dVdCQW5uN1hXdFJHbng5Q0JGZlk2SHdCY1lyK21XdHh3Vk9Ub3RHYVFZSjFMZVpoMk9LUmxBYUlBZUFwWGpRMWdhaFhNTGtNVHlneHU4UEZMMmIyblRVPSIsIm1hYyI6IjRhNzI1MGQwNWFhNTQ4NmQxMThhNTU3YzcyMjU1YTMxYTA0M2RkN2Q0MDZjM2Q1MWViMzViNGFiN2I4MDM2MjEifQ==");

		$this->assertEquals($result, false);
	}
}
