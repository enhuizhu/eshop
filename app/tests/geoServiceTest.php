<?php
  class geoServiceTest extends TestCase {
    public function testGetCoordinateFromPostCode() {
      $result = geoService::getCoordinateFromPostCode("SE18 5SA");
      $this->assertEquals($result->lat, 51.489966);
      $this->assertEquals($result->lng, 0.056635);
    }

    public function testGetDistanceBetweenPostcodes() {
      $result = geoService::getDistanceBetweenPostcodes("SE18 5SA", "SE14 6EZ");
      print_r($result);
      $this->assertEquals($result, 4.2067463884572);
    }
  }
  