<?php

class ordersServiceTest extends PHPUnit_Extensions_Database_TestCase {
	public function getConnection() {
		$pdo = new PDO("mysql:host=localhost;dbname=test","root","roowpw");
        return $this->createDefaultDBConnection($pdo, 'test');
	}
	
	public function getDataSet() {
		return $this->createFlatXMLDataSet(dirname(__FILE__).'/_files/seed.xml');
	}

	public function testConvertAddresToStr() {
		$contact = new stdClass;
		$contact->tel = '897423897';
		$contact->address1 = 't1';
		$contact->address2 = 't2';
		$contact->city = 't3';
		$contact->postcode = 't4';

		$expectResult = '897423897, t1, t2, t3, t4';

		$this->assertEquals(ordersService::convertAddresToStr($contact), $expectResult);
	}


}