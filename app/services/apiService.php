<?php

class apiService {
	
	public static function getRawData() {
		$requst = Request::instance();
		return json_decode($requst->getContent()); 
	}

	/**
	* return error message 
	**/
	public static function apiErrorResponse($msg) {
		$response = new stdClass();
		$response->success = false;
		$response->message = $msg;

		return $response;
	}

	public static function apiSuccessResponse($msgs) {
		$response = new stdClass;
		$response->success = true;

		if (is_array($msgs)) {
			foreach ($msgs as $key => $value) {
				$response->$key = $value;
			}
		}else{
			$response->message = $msgs;
		}

		return $response;
	}

	/**
	* function to generate the token
	**/
	public static function generateToken($username, $password) {
		$response = array();
		$response["username"] = $username;
		$response["password"] = $password;
		/**
		* mktime hour:min:sec year,month,day
		**/
		$response["expire"] = mktime(date('H'), date('i'), date('s'), date('m'), date('d') + 7, date('Y'));

		return Crypt::encrypt(json_encode($response));
	}

	/**
	* function to generate the token for facebook
	**/
	public static function generateSocialToken($username, $type) {
		$response = array();
		$response["username"] = $username;
		$response["type"] = $type;

		$response["expire"] = mktime(date('H'), date('i'), date('s'), date('m'), date('d') + 7, date('Y'));

		return Crypt::encrypt(json_encode($response));
	}

	/**
	* get user information from token
	**/
	public static function getUserInfoFromToken($token) {
		try {
			$jsonStr = Crypt::decrypt($token);
			$userInfo = json_decode($jsonStr);
			return $userInfo;
		} catch (Exception $e) {
			return false;
		}
		
	}

	/**
	* check if it's valid user token
	**/
	public static function isValidUserToken($token) {
		$userInfo = self::getUserInfoFromToken($token);

		if (!$userInfo || empty($userInfo->username) || empty($userInfo->expire) || time() > $userInfo->expire || (empty($userInfo->password) && empty($userInfo->type))) {
			return false;
		}

		$loginResult = customerService::login($userInfo);

		if (!$loginResult->success) {
			return false;
		}

		return true;
	}

	public static function isValidShopUserToken($token) {
		$userInfo = self::getUserInfoFromToken($token);

		if (!$userInfo || empty($userInfo->username) || empty($userInfo->expire) || time() > $userInfo->expire || (empty($userInfo->password) && empty($userInfo->type))) {
			return false;
		}

		$loginResult = usersService::apiLogin($userInfo);

		if (!$loginResult->success) {
			return false;
		}

		return true;
	}

	public static function getUserInfo($isShopUser = false) {
		$token = Input::get("token");
		$userInfo = self:: getUserInfoFromToken($token);
		
		if ($isShopUser) {
			$user = User::where("username", "=", $userInfo->username)
			->first();
		}else{
			$user = customer::where("username", "=", $userInfo->username)
			->orWhere("email", "=", $userInfo->username)
			->where("type", "=", NULL)
			->first();
		}

		return $user;
	}
}

