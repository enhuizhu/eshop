<?php
  
  class usersService {

        public static function createUser(){
            $data = array();

            $rules = array(
                "username" => 'required|unique:users|min:3|max:32',
                "shop_name" => 'required',
                "password" => 'required|min:6',
                "re-password" => 'required|min:6',
                "email" => 'required|email|unique:users'
            );
            
            $validator = Validator::make(Input::all(), $rules);

            if ($validator->fails()) {
                $messages = $validator->messages()->all();
                $data["errors"] = $messages;
            }

            if (Input::get("password") != Input::get("re-password")) {
                $passwordError = "passwords not match!";
                
                if (isset($data["errors"])) {
                    array_push($data["errors"], $passwordError);
                }else{
                    $data["errors"] = array($passwordError);
                }
            }
    
            /**
            * everything is good, should save the user
            **/
            if (empty($data["errors"])) {
                /**
                * should check if email already in data base
                **/
                if (User::where('email', '=', Input::get("email"))->count() > 0) {
                    $data["errors"] = array("email already exist!");
                }else{
                    if (Input::file("logo") != null) {
                        if (commonFunctionService::uploadImageFile(Input::file("logo"))) {
                            $logo = Input::file("logo")->getClientOriginalName();
                        }else{
                            $data["errors"] = array("pleae upload image!");
                            return $data;               
                        }   
                    }

                    $user = new User();
                    $user->username = Input::get("username");
                    $user->shop_name = Input::get("shop_name");
                    $user->email = Input::get("email");
                    $user->password = Hash::make(Input::get("password"));
                    $user->role = Input::get("roles");
                    $user->active = Input::get("status");
                    $user->token = str_random(40);

                    if (isset($logo) && !empty($logo)) {
                        $user->logo = $logo;
                    }

                    $user->currency = Input::get("currency");

                    $user->save();
                    $data["success"] = "new user has been created successfully!";
                }
            }

            return $data;
        }

        public static function updateUser($user){
            $data = array();

            /**
            * need to check if is't valid input
            **/
            $rules = array(
                "username" => 'required|min:3|max:32|alpha',
                "email" => 'required|email'
            );
            
            $validator = Validator::make(Input::all(), $rules);

            if ($validator->fails()) {
                $messages = $validator->messages()->all();
                $data["errors"] = $messages;
            }else{
                
                $infoChanged = false;

                if ($user->email != Input::get("email")) {
                    /**
                    * should check if email already exist 
                    **/
                    if (User::where('email', '=', Input::get("email"))->count() > 0) {
                        $data["errors"] = array("email already exist!");
                    }else{
                        $user->email = Input::get("email");
                        $infoChanged = true;
                    }
                }

                if ($user->shop_name != Input::get("shop_name")) {
                    /**
                    * should check if shop already exist 
                    **/
                    if (User::where('shop_name', '=', Input::get("shop_name"))->count() > 0) {
                        $data["errors"] = array("shop name already exist!");
                    }else{
                        $user->shop_name = Input::get("shop_name");
                        $infoChanged = true;
                    }
                }

                /**
                * udpate the user
                **/
                if (empty($data["errors"])) {
                    if ($user->username != Input::get("username")) {
                        $user->username = Input::get("username");
                        $infoChanged = true;
                    }

                    if ($user->role != Input::get("roles")) {
                        $user->role = Input::get("roles");
                        $infoChanged = true;
                    }

                    if ($user->active != Input::get("status")) {
                        $user->active = Input::get("status");
                        $infoChanged = true;
                    }

                    if ($user->currency != Input::get("currency")) {
                        /**
                        * should check if shop already exist 
                        **/
                        $user->currency = Input::get("currency");
                        $infoChanged = true;
                    }
                    
                    /**
                    * should check if user token is missing
                    **/
                    if (empty($user->token)) {
                        $user->token = str_random(40);
                        $infoChanged = true;
                    }

                    if (Input::file("logo") != null) {
                        if (commonFunctionService::uploadImageFile(Input::file("logo"))) {
                            $user->logo = Input::file("logo")->getClientOriginalName();
                            $infoChanged = true;
                        }else{
                            $data["errors"] = array("pleae upload image!");
                            return $data;               
                        }   
                    }


                    if ($infoChanged) {
                        $user->save();
                        $data["success"] = "user has been updated successfully!";
                    }else{
                        $data["success"] = "user info will be kept as before!";
                    }

                }
            }

            return $data;

        }

        public static function updateUserPass($id){
            $rules = array(
                "oldPassword" => 'required',
                "newPassword" => 'required|min:6',
                "repeatNewPassword" => 'required|min:6',
            );
            
            $validator = Validator::make(Input::all(), $rules);

            if ($validator->fails()) {
                $messages = $validator->messages()->all();
                $data["errors"] = $messages;
                return $data;
            }

            /**
            * should check if newPassword equal to repeatNewPassword
            **/
            if (Input::get("newPassword") != Input::get("repeatNewPassword")) {
                $data["errors"] = array("New password is not same as repeated password!");
                return $data;
            }

            /**
            * should find user with id = $id
            **/
            $user = User::find($id);

            if (empty($user)) {
                $data["errors"] = array("Can not find user with id as $id");
                return $data;
            }

            /**
            * should check if old password match the password in the database
            **/
            if (!Hash::check(Input::get("oldPassword"),$user->password)) {
                $data["errors"] = array("Old password is wrong!");
                return $data;
            }

            /**
            * everything is fine should update the password
            **/
            $user->password = Hash::make(Input::get("newPassword"));
            $user->save();

            $data["success"] = "the password for ".$user->username." has been updated successfully!";
            return $data;
        }
        
        public static function getRolesAndStatus(){
            $data = array();
            /**
            * set roles array
            **/
            $roles = array(
                'Administrator' => 2, 
                'User' => 1
            );

            $status = array(
                'Active' => 1,
                'Inactive' => 0
            );

            $data["roles"] = $roles;
            $data["status"] = $status;

            return $data;
        }

        public static function getUserId() {
            if (Auth::check()) {
                //user already login
                return Auth::user()->id;
            }else if (Request::header('shop-token')) {
                $user = self::getUserBaseOnHeaderToken();
                return $user->id;   
            }else{
                throw new Exception("user token missing", 1);
            }
        }

        public static function getUserBaseOnHeaderToken() {
            return User::where('token', Request::header('shop-token'))->first();
        }

        public static function apiLogin($loginInfo) {
            $user = User::where('username', '=', $loginInfo->username)
                ->first();

            if (empty($user)) {
                return apiService::apiErrorResponse($loginInfo->username . " does not exist!");
            }

            /**
            * everything goes well, should generate the token
            **/
            if (Hash::check($loginInfo->password, $user->password)){
                $token = apiService::generateToken($loginInfo->username, $loginInfo->password);
                return apiService::apiSuccessResponse(array("token" => $token));
            }else{
                return apiService::apiErrorResponse("password is wrong.");
            }
        }
  }

