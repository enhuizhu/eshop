<?php
use Illuminate\Support\Facades\Redis;

class ordersService {
	public static function saveOrders($orderInfo) {
		try{
			$contactId = self::saveContact($orderInfo->address, apiService::getUserInfo()->id);
			$newOrder = new orders;
			$newOrder->deliver_time = time();
			$newOrder->order_time = time();
			$newOrder->items = json_encode($orderInfo->items);
			$newOrder->status = '0';
			$newOrder->read = '0';
			$newOrder->note = $orderInfo->note;
			$newOrder->contact_id = $contactId;
			$newOrder->user_id =  apiService::getUserInfo()->id;
			$newOrder->save();
			/**
			* new order come should pulish event
			**/
			Redis::publish(Config::get('constants.PLACE_ORDER'), $newOrder->id);
			
			return apiService::apiSuccessResponse('order has been saved successfully!');
		}catch(Exception $e){
			return apiService::apiErrorResponse($e->getMessage());
		}
	}

	public static function convertAddresToStr($object) {
		$str = "";
		$keys = array('tel', 'address1', 'address2', 'city', 'postcode');

		foreach ($keys as $key => $value) {
			$str .= $object->{$value} . ', ';
		}

		return substr($str, 0, -2);
	}

	public static function getOrderById($id) {
		$isApiUser = !empty(Input::get('token'));
		/**
		* user not login
		**/
		if (!apiService::isValidShopUserToken(Input::get('token')) && !Auth::check()) {
			return '';
		}else{
			$user = $isApiUser ? apiService::getUserInfo(true) :Auth::user();
		}

		$order = orders::find($id);
		$items = json_decode($order->items);
		$returnData = [];

		$userInfo = $order->customer()->find($order->user_id);
		$shopUserId = $userInfo->shop_user_id;

		if ($shopUserId != $user->id) {
			return '';
		}

		$contactInfo = $order->contact()->first();

		$productsWithQuantity = [];

		foreach ($items as $key => $value) {
			array_push($productsWithQuantity, productService::getProductWithQuantity($value->id, $value->quantity));
		}

		$returnData['products'] = $productsWithQuantity;
		$returnData['userInfo'] = array(
			'username' => $userInfo->username,
			'email' => $userInfo->email
		);

		$returnData['contact'] = $contactInfo;
		$returnData['orderTime'] = $order->order_time;
		$returnData['delieverTime'] = $order->deliver_time;
		$returnData['read'] = $order->read;
		$returnData['status'] = $order->status;
		$returnData['id'] = $order->id;
		
		return $returnData;
	}

	public static function updateOrderStatus($orderInfo) {
		/**
		* find the order
		**/
		$order = orders::find($orderInfo->id);

		if (empty($order)) {
			return apiService::apiErrorResponse('order with id ' . $orderInfo->id . ' is not exist.');
		}
		
		try{
			$order->status = $orderInfo->status;
			$order->save();
		}catch(Exception $e) {
			return apiService::apiErrorResponse($e->getMessage());
		}

		return apiService::apiSuccessResponse('ok');
	}

	public static function saveContact($address, $userId) {
		/**
		* should check if the address is already in the db, if
		* it's already in the database, should update it
		**/
		$contacts = contact::where('user_id', '=', $userId)->get();
		
		/**
		* should check if it's the same address
		**/
		$newAddStr = self::convertAddresToStr($address);
		$matchContact = null;

		foreach ($contacts as $key => $contact) {
			if (self::convertAddresToStr($contact) == $newAddStr) {
				$matchContact = $contact;
				break;
			}
		}

		if (!empty($matchContact)) {
			return $matchContact->id;
		}

		$contact = new contact;
		$contact->tel = $address->tel;
		$contact->address1 = $address->address1;
		$contact->address2 = $address->address2;
		$contact->city = $address->city;
		$contact->postcode = $address->postcode;
		$contact->user_id = $userId;
		$contact->save();

		return $contact->id;
	}
}
