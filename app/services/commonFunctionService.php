<?php 
/**
*  all the common functions are here
**/
class commonFunctionService{
	/**
	* function to upload pic
	**/
	public static function uploadImageFile($imageFile){
		if (!empty($imageFile)) {
			if (strpos($imageFile->getMimeType(), "image") === false) {
				return false;
			}

			/**
			* everything is fine should update the database
			**/
			$fileName = $imageFile->getClientOriginalName();
			$destinationPath = "uploads/";

			$imageFile->move($destinationPath, $fileName);

			return true;
	}
	
	return false;
}

	/**
	* function to ouput paginations
	**/
	public static function paginations($totalPages, $currentPage, $baseLink = null){
		$content = '
			  	<nav class="pull-right">
				  <ul class="pagination">';
		
		$previousClass = $currentPage == 1 ? ' class="disabled"' : '';
	    $content.='<li'.$previousClass.'><a href="'.url("$baseLink/".($currentPage == 1 ? $currentPage : $currentPage - 1)).'" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';

        for ($i=1; $i <= $totalPages ; $i++) { 
			$currentClass = $currentPage == $i ? ' class="active"' : '';
			$content.= '<li'.$currentClass.'><a href="'.url("$baseLink/".$i).'">'.$i.' <span class="sr-only">(current)</span></a></li>';
	    }				    
		
		$nextClass = $currentPage == $totalPages ? ' class="disabled"' : '';
		$content.='<li'.$nextClass.'><a href="'.url("$baseLink/".($currentPage == $totalPages ? $currentPage : $currentPage + 1)).'" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';

		$content.= '
				  </ul>
				</nav>
				<div class="clearfix"></div>
			  ';
		
		return $content;
	}

	/**
	* function to get input filed default value
	**/
	public static function getFieldDefaultValue($dbData,$dbKey,$inputKey,$reset = false){
		if ($reset) {
			return "";
		}
		
		$defaultValue = !empty($dbData) ? $dbData[$dbKey] : Input::get($inputKey);
		return $defaultValue;
	}

	

}