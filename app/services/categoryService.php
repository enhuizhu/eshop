<?php

class categoryService{
	
	public static $basicCategory = array(
		"0" => "root"
	);
	
	public static function allCategoryies() {
		return category::where("user_id", usersService::getUserId())->get();
	}

	public static function allCategoryieIds() {
		$categories = self::allCategoryies()->toArray();

		return array_map(function($v) {
			return $v["id"];
		}, $categories);        
	}

	public static function getCategories(){
		$categories = self::allCategoryies();

		if (!empty($categories)) {
			$categoryArray = self::convertCategoriesToArr($categories);
			$basicCategory = self::mergeArr(self::$basicCategory, $categoryArray);
			ksort($basicCategory);
		}

		return $basicCategory;
	}

	public static function getCategoriesWithIdAsKey(){
		 $categoryArray = self::allCategoryies()->toArray();
		 return self::convertCategoryToArrWithIdAsKey($categoryArray);
	}
	
	public static function convertCategoryToArrWithIdAsKey($categoryArray){
		$categoryWithId = array();
		/**
		* turn the category into key value
		**/
		foreach ($categoryArray as $category) {
				$categoryWithId[$category["id"]] = $category;
		}

		return $categoryWithId;
	}


	public static function convertCategoriesToArr($categories){
		if (!empty($categories)) {
			$categoryArray = $categories->toArray();
			$categoryWithId = self::convertCategoryToArrWithIdAsKey($categoryArray);
			
			$tempArr = array();
			foreach ($categoryArray as $category) {
				$tempArr[$category["id"]] = self::getMenuNameWithParentName($categoryWithId, $category);
			}

			return $tempArr;
		}

		return false;
	}

	/**
	* get the category name that with parent's name
	**/
	public static function getMenuNameWithParentName($categories, $selectedCategory){
		try{
			$currentParent = $categories[$selectedCategory["parent_id"]];
		}catch(Exception $e){
			$currentParent = null;
		}

		$categoryName = empty($currentParent) ? $selectedCategory["name"] : $currentParent["name"]."/".$selectedCategory["name"];
		
		while (!empty($currentParent) && $currentParent["parent_id"]!=0) {
			$currentParent = $categories[$currentParent["parent_id"]];
			$categoryName = $currentParent["name"]."/".$categoryName;
		}

		return "root/".$categoryName;
	}

	public static function mergeArr($arr1, $arr2){
		foreach ($arr1 as $key => $value) {
			$arr2[$key] = $value;
		}

		return $arr2;
	}

	public static function categoriesWithoutChildren($id){
		$categories  = category::where("parent_id","<>",$id)->where("id","<>",$id)->where("user_id", Auth::user()->id)->get();
		$tempArr = self::convertCategoriesToArr($categories);
		  
		$categories = self::mergeArr(self::$basicCategory, $tempArr);
		ksort($categories);

		return $categories;
	}

	public static function getCategoryNoRoot($withLongName = false){
		$categories = self::allCategoryies()->toArray();
		/**
		* convent the array with key and value
		**/
		$newCategories = self::convertCategoryToArrWithIdAsKey($categories);

		if ($withLongName) {
			$newArr = array();
			
			foreach ($categories as $key => $value) {
			    $newArr[$value["id"]] = self::getMenuNameWithParentName($newCategories, $value);
			}

			return $newArr;
		}

		return $newCategories;
	}

	/**
	* function to update the category
	**/
	public static function updateCategory($category){
		$data = array();

		$rules = array(
			"parent_id" => 'required|integer',
			"name" => 'required'
		);

		$validator = Validator::make(Input::all(),$rules);

		if ($validator->fails()) {
			$messages = $validator->messages()->all();
			$data["errors"] = $messages;
			return $data;
		};

		if (empty($data["errors"])) {
			/**
			* everything is ok, should update the record
			**/
			if ($category->parent_id != Input::get("parent_id")) {
				$category->parent_id = Input::get("parent_id");
			}

			if ($category->name!=Input::get("name")) {
				/**
				* shoulc check if the new category name already exist
				**/
				if (self::isCategoryExist(Input::get("name"))) {
					$data["errors"] = array(Input::get("name")." already exist!");
					return $data;
				}

				$category->name = Input::get("name");
			}

			/**
			* should check if user upload new pic
			**/
			if (Input::file("pic")) {
				  if (commonFunctionService::uploadImageFile(Input::file("pic"))) {
				  	  $category->pic = Input::file("pic")->getClientOriginalName();
				  }else{
				  	  $data["errors"] = array("please upload a image!");
				  	  return $data;
				  }
			}

			$category->save();
			$data["success"] = "category has been updated successfully!";
		}

		return $data;
	}


	/**
	* function to create category
	**/
	public static function createCategory(){
		$data = array();
		
		$rules = array(
			"parent_id" => 'required|integer',
			"name" => 'required',
			"pic" => 'required'
		);
		
		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			$messages = $validator->messages()->all();
			$data["errors"] = $messages;
			return $data;
		}

		if (self::isCategoryExist(Input::get("name"))) {
    		$data["errors"] = array(Input::get("name")." already exist");
    		return $data;
    	}
		
		if (empty($data["errors"])) {
		    if (commonFunctionService::uploadImageFile(Input::file("pic"))) {
		    	$category = new category();
		    	$category->name = Input::get("name");
		    	$category->pic = Input::file("pic")->getClientOriginalName();
		    	$category->parent_id = Input::get("parent_id");
		    	$category->user_id =  Auth::user()->id;
		    	$category->save();
		    	$data["success"] = "category has been created successfully!";
		    }else{
	    		$data["errors"] = array("please upload image!");
	    		return $data;

		    }
		}

		return $data;
	}

	/**
	* function to check if the category already exist
	**/
	public static function isCategoryExist($categoryName){
		$numberOfCategory = category::where("name",$categoryName)->where("user_id", Auth::user()->id)->count();
		return $numberOfCategory > 0 ? true : false;
	}

	public static function getCategoryAndSubCategoies($categoryId){
		$ids = array(intval($categoryId));
		$ids = array_merge($ids, self::getAllSubCategoryIds($categoryId));
		return $ids;
	}

	public static function getAllSubCategoryIds($categoryId){
		  $ids = array();

		  $subcategories = category::where("parent_id",$categoryId)->get()->toArray();

		  if (empty($subcategories)) {
		  	  return $ids;
		  }

		  foreach ($subcategories as $key => $value) {
		  		array_push($ids, $value["id"]);
		  		$ids = array_merge($ids, self::getAllSubCategoryIds($value["id"]));
		  }

		  return $ids;
	}

	/**
	* get categories in ids
	**/
	public static function getCategoriesInIds($ids){
		return category::whereIn("id", $ids);
	}
}