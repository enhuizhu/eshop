<?php
/**
* service to control products
**/
class productService{
	
	public static $recordPerPage = 10;
	
	public static $currency = "&pound;";

	/**
	* functon to generate currency
	**/
	public static function generateCurrency($price){
		 $currency = '';
		 
		 if (!empty(Auth::user()->currency)) {
			 $currency = Config::get('constants.CURRENCIES')[Auth::user()->currency];
		 }

		 return $currency.$price;
	}

	/**
	* function to add product
	**/
	public static function addProduct(){
		$data = array();

		$validateResult = self::validateInput();

		if ($validateResult != true) {
			return $validateResult;
		}
		/**
		* should check if the image is ok
		**/
		if (Input::file("pic") == null) {
			$data["errors"] = array("please upload image!");
			return $data;
		}

		if (commonFunctionService::uploadImageFile(Input::file("pic"))) {
			if (self::isProductExist(Input::get("name"))) {
				$data["errors"] = array(Input::get("name")." already exist!");
				return $data;
			}
			/**
			* everything is fine, should save this product
			**/
			$product = new products();
			$product->name = Input::get("name");
			$product->description = Input::get("description");
			$product->price = Input::get("price");
			$product->category_id = Input::get("category_id");
			$product->pics = Input::file("pic")->getClientOriginalName();
			$product->save();
			$data["success"] = "new product has been created successfully!";
		}else{
			$data["errors"] = array("please upload image!");
		}

		return $data;
	}

	public static function validateInput(){
		$rules = array(
			"name" => "required",
			"description" => "required",
			"price" => "required",
			"category_id" => "required|integer"
		);

		$validator = Validator::make(Input::all(),$rules);

		if ($validator->fails()) {
			$messages = $validator->messages()->all();
			$data["errors"] = $messages;
			return $data;
		}

		return true;
	}

	/**
	* function to update the product
	**/
	public static function updateProduct($product){
		$data = array();

		$validateResult = self::validateInput();

		if ($validateResult != true) {
			return $validateResult;
		}

		if (Input::file("pic") != null) {
			if (commonFunctionService::uploadImageFile(Input::file("pic"))) {
				$product->pics = Input::file("pic")->getClientOriginalName();
			}else{
				$data["errors"] = array("pleae upload image!");
				return $data;     			
			}   	
		}

		if (Input::get("name") != $product->name) {
			$product->name = Input::get("name");
		}

		if (Input::get("description") != $product->description) {
			$product->description = Input::get("description");
		}

		if (Input::get("category_id") != $product->category_id) {
			$product->category_id = Input::get("category_id");
		}

		if (Input::get("price") != $product->price) {
			$product->price = Input::get("price");
		}

		$product->save();

		$data["success"] = "product has been updated successfully";

		return $data;

	}

	/**
	* function to check if product already exist
	**/
	public static function isProductExist($productName){
		$numberOfProducts = products::where("name",$productName)->count();
		return $numberOfProducts > 0 ? true : false;
	}

	
	public static function getTotalPages($filters){
		$recordPerPage = self::$recordPerPage;
		
		if (empty($filters)) {
			$totalPages = ceil(products::all()->count()/$recordPerPage);
		}else{
			$index = 0;
			
			foreach ($filters as $key => $value) {
				if ($index == 0) {
					$products = self::getProductsWithFilter(false, $key, $value, true);
				}else{
					$products = self::getProductsWithFilter($products, $key, $value);
				}

				$index++;
			}

			$totalPages = ceil($products->count()/$recordPerPage);
		}

		return $totalPages;
	}

	public static function getProductsWithFilter($products, $key, $value, $isFirst = false){
		if (is_numeric($value)) {				
			if ($key == "category_id") {
				$ids = categoryService::getCategoryAndSubCategoies($value);
				$products =  $isFirst ? products::whereIn($key, $ids) : $products->whereIn($key, $ids);
			}else{
				$products = $isFirst ? products::where($key, $value) : $products->where($key, $value);
			}				
		}else{
			$products = $isFirst ? products::where($key, "LIKE", "%".$value."%") : $products->where($key, "LIKE", "%".$value."%");
		}

		return $products;
	}

	/**
	* function to get all the products
	**/
	public static function getAllProducts($pageNumber = 1, $cateroies = null, $key = null){
		// die(var_dump(categoryService::allCategoryieIds()));
		/**
	 * get total pages
	 **/
	  $recordPerPage = self::$recordPerPage;
		
	  $filters = array();

		if (Session::has("category_id")) {
			$filters["category_id"] = Session::get("category_id");
		}	 

		if (Session::has("name")) {
			$filters["name"] = Session::get("name");
		}

		if (!empty($key)) {
			$filters["name"] = $key;
		}

		// $totalPages = self::getTotalPages($filters);		

		$products = products::whereIn('category_id', (empty($cateroies) || empty($cateroies[0])) ? categoryService::allCategoryieIds() : $cateroies);

		foreach ($filters as $key => $value) {
			$products = self::getProductsWithFilter($products, $key, $value);
		}

		$totalPages = ceil($products->count() / $recordPerPage);
			
		$products = $products->take($recordPerPage)->offset(($pageNumber-1)*$recordPerPage)->get()->toArray();

		return array(
			"totalPages" => $totalPages,
			"products" => $products,
			"currentPage" => $pageNumber
		);
	}

	public static function getProductsInCategoryIds($categoryIds){
		$products = products::whereIn("category_id", $categoryIds);
		return $products;
	}

	public static function getProductWithQuantity($id, $quantity = 0) {
		$keys = array('name','price','pics');
		$product = products::find($id);
		$convertedProduct = array();

		foreach ($keys as $key => $value) {
			$convertedProduct[$value] = $product->{$value};
		}
		
		$convertedProduct['quantity'] = $quantity;

		return $convertedProduct;
	}

	public static function getProductsWithCategory($token) {
		// need get all the categories under that token
		$user = User::where('token', $token)->first();
		
		if (empty($user)) {
			return apiService::apiErrorResponse('shop token is wrong!');
		}

		$shopInfo = new stdClass();
		$shopInfo->shopName = $user->shop_name;
		$shopInfo->logo = $user->logo;
		$shopInfo->currency = $user->currency;
		
		$userId = $user->id;
		// get all the categories under this id
		$cateroies = category::where('user_id', $userId)->get();

		foreach($cateroies as $cat) {
			$cat->products;
		}

		$shopInfo->categories = $cateroies;

		return $shopInfo;
	}
}


