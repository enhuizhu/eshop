<?php

class customerService {
	public static function isUserExist($email) {
		$count = customer::where('email', '=', $email)->count();
		return $count > 0;
	}

	public static function createUser($userInfo) {
		/**
		* should check if it's valid $userInfo
		**/
		try{
			$validator= Validator::make(
				array(
					'username' => $userInfo->username,
					'email' => $userInfo->email,
					'password' => $userInfo->password
				),
				array(
					'username' => 'required|min:3|unique:customer,username',
					'email' => 'required|email|unique:customer,email',
					'password' => 'required|min:6'
				)
			);	
		} catch (Exception $e) {
			return apiService::apiErrorResponse($e->getMessage());
		}
        
		
		$response = new stdClass();
			if ($validator->fails()) {
				$messages = $validator->messages();
				$response->success = false;
				$response->messages = $messages;

				return $response;
			}

			$newUser = new customer;
			$newUser->username = $userInfo->username;
			$newUser->email = $userInfo->email;
			$newUser->password= Hash::make($userInfo->password);
			$newUser->shop_user_id = usersService::getUserId();

			$saved = $newUser->save();

			if ($saved) {
				$response->success = true;
			}else{
				$response->success = false;
				$response->messages = json_encode(array("database" => array("can not save user info into database.")));
			}

			return $response;
	}

	public static function loginWithSocialAccount($loginInfo) {
		$newUser = new customer;
	}

	public static function login($loginInfo) {
		$customer = customer::where("username", "=", $loginInfo->username)
			->orWhere("email", "=", $loginInfo->username)
			->first();
		$isSocialUser = isset($loginInfo->type) && !empty($loginInfo->type) ? true : false;
		
		if (empty($customer)) {
			/**
			* should check if it try to login from social net work
			**/
			if ($isSocialUser) {		        
				$newUser = new customer;
				$newUser->username = $loginInfo->username;
				$newUser->email = $loginInfo->email;
				$newUser->type = $loginInfo->type;
				$newUser->save();
			}else{
				return apiService::apiErrorResponse($loginInfo->username . " does not exist!");
			}
		}

		if ($isSocialUser) {
			$token = apiService::generateSocialToken($loginInfo->username, $loginInfo->type);
			return apiService::apiSuccessResponse(array("token" => $token, "username" => $loginInfo->username));
		}

		/**
		* everything goes well, should generate the token
		**/
		if (Hash::check($loginInfo->password, $customer->password)){
			$token = apiService::generateToken($loginInfo->username, $loginInfo->password);
			return apiService::apiSuccessResponse(array("token" => $token, "username" => $customer->username));
		}else{
			return apiService::apiErrorResponse("password is wrong.");
		}
	}

}