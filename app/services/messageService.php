<?php
 class message{

 	public static function errors($errors){
 		if(isset($errors) && !empty($errors)):
		  foreach($errors as $error):
?>

				<div class="alert alert-danger alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <?php
				  	echo $error;
				  ?>
				</div>					    

	<?php
			endforeach;
	  endif;
 	}


 	public static function success($success){
 		if (isset($success) && !empty($success)) {
?>
		<div class="alert alert-success alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<?php
					echo $success;
				?>
		</div>				
<?php 		
 		}
 	}

 	public static function info($message){
 ?>
		<div class="alert alert-info alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		    <?php
		    	echo $message;
		    ?>
		</div>					
<?php
 	}
 }
?>