<?php

class geoService {
  public static function degreeToRadians($degrees) {
    return $degrees * pi() / 180;
  }

  public static function distanceInMilesBetweenCoordinates($lat1, $lon1, $lat2, $lon2) {
    $earthRadiumMile = 3958.8;
    $dLat = self::degreeToRadians($lat2 - $lat1);
    $dLon = self::degreeToRadians($lon2 - $lon1);
    $lat1 = self::degreeToRadians($lat1);
    $lat2 = self::degreeToRadians($lat2);

    $a = sin($dLat / 2) * sin($dLat / 2)
      + sin($dLon / 2) * sin($dLon / 2) * 
        cos($lat1) * cos($lat2);
    $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
    return $earthRadiumMile * $c;
  }

  public static function curlGetContents($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
  }

  public static function getCoordinateFromPostCode($postcode) {
    $url = "http://open.mapquestapi.com/geocoding/v1/address?key=FmsY9A69QagHXRQcThMZA6Fycf4d18LS&location=".urlencode($postcode);
    $result = self::curlGetContents($url);
    $result = json_decode($result);
    return $result->results[0]->locations[0]->latLng;
  }

  public static function getDistanceBetweenPostcodes($postcode1, $postcode2) {
    $latLng1 = self::getCoordinateFromPostCode($postcode1);
    $latLng2 = self::getCoordinateFromPostCode($postcode2);

    return self::distanceInMilesBetweenCoordinates($latLng1->lat, $latLng1->lng, $latLng2->lat, $latLng2->lng);
  }
}