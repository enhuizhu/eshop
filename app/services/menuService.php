<?php
class menuService {
	
	public static $menus = array(
		"User Management" => array(
			"icon" => "glyphicon-user",
			"items" => array("Add New User" => array(
				"icon" => "glyphicon-plus-sign",
				"url" => "admin/createAdmin",
			),
			"Manage Users" => array(
				"icon" => "glyphicon-th",
				"url" => "admin/manageAdmin"
			),
		  ),
			"keyLable" => "users",
			"adminOnly" => true
		 ),

		 "Category Management" => array(
		 		"icon" => "glyphicon-list-alt",
		 		"items" => array("Add New Category" => array(
					"icon" => "glyphicon-plus-sign",
					"url" => "admin/products/createCategory"
					),
					'Manage Categories' => array(
						"icon" => "glyphicon-th",
						"url" => "admin/products/manageCategory",
					),
		    ),
		 	"keyLable" => "categories",
		 ),

		 "Products Management" => array(
				"icon" => "glyphicon-scale",
				"items" => array("Add New Product" => array(
					"icon" => "glyphicon-plus-sign",
					"url" => "admin/products/createProduct",
					),
					"Manage Products" => array(
						"icon" => "glyphicon-th",
						"url" => "admin/products/manageProducts",
					)
				),
				"keyLable" => "products"		 
			),

			"Live Orders" => array(
				"icon" => "glyphicon-th-list",
				"items" => array("Live Orders" => array(
					"icon" => "glyphicon-th-list",
					"url" => "admin/liveOrder",
				)
				),
				"keyLable" => "Orders"        
			),

			"Business Time" => array(
				"icon" => "glyphicon-time",
				"items" => array(
					"Business Time" => array(
						"icon" => "glyphicon-time",
						"url" => "admin/time",
					),
					"Turn on or off the store" => array(
						"icon" => "glyphicon-shopping-cart",
						"url" => "admin/turn-on-or-off-store",
					)
				),
				"keyLable" => "Times"     
			),

			"Delivery Management" => array(
				"icon" => "glyphicon-wrench",
				"items" => array(
					"Set Delivery Distance" => array(
						"icon" => "glyphicon-wrench",
						"url" => "admin/set-delivery-distance",
					),
					"Set Delivery Fees" => array(
						"icon" => "glyphicon-gbp",
						"url" => "admin/delivery",
					),
				),
				"keyLable" => "Delivery"     
			),
		);

    public static function generateMenu(){
        $content = '<ul class="nav in" id="side-menu">';
        
        foreach (self::$menus as $key => $value) {
        	 /**
        	 * should check if it's admin or not
        	 **/
        	 if (!userPermission::isAdmin() && !empty($value["adminOnly"]) && $value["adminOnly"]) {
        	 	continue;
        	 }

        	 $id = strtolower(str_replace(" ", "", $key)); 
        	 
        	 $content.='<li>
                        <a href="#" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample" data-target="#'.$id.'">
                         <span class="glyphicon '.$value["icon"].'"></span>'.$key.'<span class="glyphicon glyphicon-chevron-right pull-right"></span>
                        </a>
                        <ul class="nav nav-second-level collapse" id="'.$id.'">';
                            
             foreach ($value["items"] as $k => $v) {
                $content.=' <li>
                                <a href="'.url($v["url"]).'"><span class="glyphicon '.$v["icon"].'"></span>'.$k.'</a>
                            </li>';
             }

             $content.='</ul></li>'; 
        }

        $content.='</ul>';

        return $content;
    }


}

