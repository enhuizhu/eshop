<?php

class CategoryController extends BaseController {
	
	public function get()
	{
		$categories = categoryService::getCategoryNoRoot();
		return Response::json($categories);
	}
}