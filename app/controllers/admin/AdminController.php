<?php

class AdminController extends \BaseController {

	public function __construct(){
		   $this->beforeFilter("auth");
	}
	
	public function dashBoard(){
		return View::make("admin/dashBoard");
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function createAdmin()
	{
		if (!userPermission::isAdmin()) {
			die("you don't have permission to create user!");
		}

		$data = array();

		$statusAndRoles = usersService::getRolesAndStatus();

		$data = array_merge($data, $statusAndRoles);

		$data["currencies"] = Config::get('constants.CURRENCIES');

		if (Request::isMethod("post")) {
			$result = usersService::createUser();
			$data = array_merge($data, $result);
		}

		return View::make("admin/createAdmin", $data);
	}

	/**
	* function to maname users
	**/
	public function manageAdmin()
	{
		/**
		* get all the users
		**/
		$data = array();

		$users = User::all()->toArray();

		$data["users"] = $users;

		if (Session::has("message")) {
			$data["message"] = Session::get("message");
		}

		return View::make("admin/updateAdmin", $data);
	}

	public function updateAdmin($id){
		/**
		* should check if user is administrator
		**/
		if (!userPermission::isAdmin()) {
			die("you don't have permission to edit user");
		}


		$data = array();
		$data["mode"] = "update";
		$data["currencies"] = Config::get('constants.CURRENCIES');

		$statusAndRoles = usersService::getRolesAndStatus();
		$data = array_merge($data, $statusAndRoles);

		if (empty($id)) {
		    die("user id can not be empty!");
		}else{
			$user = User::find($id);

			if (empty($user)) {
			   die("can not find user whose user id is $id");
			}else{
			   if (Request::isMethod("post")) {
					 $result = usersService::updateUser($user);
					 $data = array_merge($data, $result);
				 }
				 
			   $data["user"] = $user;
			}
		}

		return View::make("admin/createAdmin", $data);
	}

	public function updateAdminPass($id){
		/**
		* should check if user is administrator
		**/
		if (!userPermission::isAdmin()) {
			die("you don't have permission to edit user");
		}

		$user = User::find($id);

		if (empty($user)) {
			die("can not find user whose id is $id");
		}

		$data = array(
			"user" => $user
		);

		if(Request::isMethod("post")){
			$result = usersService::updateUserPass($id);
			$data = array_merge($data, $result);
		}

		return View::make("admin/updateAdminPass", $data);
	}

	public function deleteAdmin($id){
		$user = User::find($id);
		
		if (!empty($user)) {
			$username = $user->username;
			$user->delete();
			return Redirect::to("admin/manageAdmin")->with("message","$username has been deleted successfully!");
		}else{
			return Redirect::to("admin/manageAdmin")->with("message", "empty user!");
		}
	}
}
