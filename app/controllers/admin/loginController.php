<?php
   
class loginController extends \BaseController{

	public function login(){
		$this->beforeFilter("alreadyLogin");

		if (Request::isMethod("post")) {
			$userdata = array(
				"username" => Input::get("username"),
				"password" => Input::get("password")
			);

			if (Auth::attempt($userdata)) {
				return Redirect::to('admin/');
			} else {
				return Redirect::to('admin/login')->with('login_errors', 'invalid user name or password');
			}

		}

		return View::make("admin/login");
	}

	/**
	* logout user
	**/
	public function logout(){  
		Auth::logout();  
		return Redirect::to('admin/login');  
	}
}
