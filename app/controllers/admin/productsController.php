<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class productsController extends \BaseController {
	public function __construct(){
		$this->beforeFilter("auth");
	}

	public function install() {
		$this->installProducts();		
		$this->installCategory();
	}
	
	/**
	* functon to create products table
	**/
	protected function installProducts(){
		if (Schema::hasTable('products')) {
			echo "products table already exist!<br>";
			return false;
		}

		echo "create products table ...<br>";
		Schema::create("products", function($table){
			$table->increments('id');
			$table->string("name", 255);
			$table->text("description");
			$table->float("price");
			$table->integer("category_id");
			$table->text("pics");
			$table->string("feature_pic", 255);
			$table->timestamps();
		});
	}

	/**
	* function to create category table
	**/
	protected function installCategory(){
		if (Schema::hasTable('category')) {
			echo "category table already exist!<br>";
			return false;
		}

		echo "create category table ...<br>";
		Schema::create("category", function($table){
				$table->increments("id");
				$table->string("name", 255);
				$table->string("pic", 255);
				$table->integer("parent_id");
				$table->integer("order");
				$table->timestamps();
		});
	}

	/**
	* function to create the category
	**/
	public function createCategory(){
		// categoryService::isCategoryExist("starter");
		
		$data = array();
		
		if (Request::isMethod("post")) {
			$result = categoryService::createCategory();
			$data = array_merge($data,$result);
		}

		$categories = categoryService::getCategories();

		$data["categories"] = $categories;

		return View::make("admin/createCategory", $data);
	}

	/**
	* function to manage the category
	**/
	public function manageCategory(){
		$data = array();

		$data["categories"] = categoryService::getCategoryNoRoot();

		if (Session::has("message")) {
			$data["message"] = Session::get("message");
			Session::forget("message");
		}

		return View::make("admin/manageCategory", $data);
	}

	/**
	* function to update the category
	**/
	public function updateCategory($id){
		$data = array();

		$category = category::find($id);

		if (empty($category)) {
			die("can not fine category with id:$id");
		}

		$data["category"] = $category;

		$data["categories"] = categoryService::categoriesWithoutChildren($id);

		/**
		* check if it has post the form, if it is then update the category
		**/
		if (Request::isMethod("post")) {
			$result = categoryService::updateCategory($category);
			$data = array_merge($data, $result);
		}

		
		return View::make("admin/createCategory", $data);
	}


	/**
	* function to delete the category
	**/
	public function deleteCategory($id){
		/**
		* get the categoy with category id is $id, and delete all it's children categories
		**/
		$category = category::find($id);
		$categoryIds = categoryService::getCategoryAndSubCategoies($id);
		$categories = categoryService::getCategoriesInIds($categoryIds);

		if (!empty($categories) && !empty($category)) {
			$categoryName = $category->name;
			/**
			* should get all the products in that categories
			**/
			$products = productService::getProductsInCategoryIds($categoryIds);
			/**
			* delete all the products
			**/
			$products->delete();

			/**
			* delete all the $categories
			**/
			$categories->delete();

			return Redirect::to("admin/products/manageCategory")->with("message","$categoryName has been deleted successfully!");
		}else{
			return Redirect::to("admin/products/manageCategory")->with("message", "empty category!");
		}

	}

	/**
	* function to add new product
	**/
	public function addProduct(){
		$data = array();
		
		if (Request::isMethod("post")) {
			$result = productService::addProduct();
			$data = array_merge($data, $result);
		}

		/**
		* should get all the categories
		**/
		$data["categories"] = categoryService::getCategoryNoRoot(true);

		return View::make("admin/addProduct", $data);
	}

	/**
	* function to display all the products
	**/
	public function manageProducts($pageNumber = 1){		
		$data = array();
		$data["pageNumber"] = $pageNumber;

		/**
		* check if user already set filters
		**/
		if (Request::isMethod("post")) {
			$category = Input::get("filterCategory");
			$keyWords = Input::get("keyWord");

			if ($category != 0) {
				Session::put("category_id",$category);
			}else{
				Session::forget("category_id");
			}

			if (!empty($keyWords)) {
				Session::put("name",$keyWords);
			}else{
				Session::forget("name");
			}

			return Redirect::to("admin/products/manageProducts");
		}

		// dd("here!");
		$products = productService::getAllProducts($pageNumber);
		// dd("products!");
		if (Session::has("message")) {
			$data["success"] = Session::get("message");
			Session::forget("message");
		}
		         
		$data = array_merge($data, $products);

		return View::make("admin/manageProducts", $data);
	}

	/**
	* function to update the product
	**/
	public function updateProduct($productId){
		$data = array();

		/**
		* get the product
		**/
		$product = products::find($productId);

		// dd($product);
		if (empty($product)) {
			dd("can not find product with product id : $productId");
		}

		if (Request::isMethod("post")) {
			$result = productService::updateProduct($product);
			$data = array_merge($data,$result);
		}

		$data["product"] = $product;
		/**
		* should get all the categories
		**/
		$data["categories"] = categoryService::getCategoryNoRoot(true);


		return View::make("admin/addProduct", $data);
	}

	/**
	* function to delete product
	**/
	public function deleteProduct($productId){
		$product = products::find($productId);
		$name = $product->name;
		$product->delete();

		return Redirect::to("admin/products/manageProducts")->with("message", "$name has been deleted successfully!");
	}

	// getTokenRelatedProducts() {
	// 	productService::getProductsWithCategory('a76LGldJJpOi7U1KRJTBOTNfnY2bPYAcCpObPgl8');
	// }
}
