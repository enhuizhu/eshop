<?php
   
class timeController extends \BaseController{
  public function __construct(){
    $this->beforeFilter("auth");
    // $this->installTimeTable();
  }

  protected function updateBusinessTime($businessTime) {
    $businessTime->openTime = json_encode(Input::all());
    $businessTime->updated_at = time();
    $businessTime->save();
  }

  protected function createBusinessTime() {
    $businessTime = new businessTime();
    $businessTime->openTime = json_encode(Input::all());
    $businessTime->shopUserId = Auth::user()->id;
    $businessTime->created_at = time();
    $businessTime->updated_at = time();
    $businessTime->save();
  }
  
  public function index() {
    $viewData = array();
    $viewData['success'] = NULL;
    $data = businessTime::where('shopUserId', '=', Auth::user()->id)->first();
    
    $defaultTime = '00:00';
    $defaultClose = false;
    
    $businessTime = array(
      'Monday' => array(
        'start' => $defaultTime,
        'end' => $defaultTime,
        'close' => $defaultClose,
      ),
      'Tuesday' => array(
        'start' => $defaultTime,
        'end' => $defaultTime,
        'close' => $defaultClose,
      ),
      'Wednesday' => array(
        'start' => $defaultTime,
        'end' => $defaultTime,
        'close' => $defaultClose,
      ),
      'Thursday' => array(
        'start' => $defaultTime,
        'end' => $defaultTime,
        'close' => $defaultClose,
      ),
      'Friday' => array(
        'start' => $defaultTime,
        'end' => $defaultTime,
        'close' => $defaultClose,
      ),
      'Saturday' => array(
        'start' => $defaultTime,
        'end' => $defaultTime,
        'close' => $defaultClose,
      ),
      'Sunday' => array(
        'start' => $defaultTime,
        'end' => $defaultTime,
        'close' => $defaultClose,
      ),
    );

    if (Request::isMethod('post')) {
      if (empty($data)) {
        $this->createBusinessTime();
        $viewData['success'] = 'Business time has been created successfully!';   
      } else {
        $this->updateBusinessTime($data);
        $viewData['success'] = 'Business time has been updated successfully!';   
      }

      $viewData['businessTime'] = Input::all();
    } else {
      if (empty($data) || empty($data->openTime)) {
        $viewData['businessTime'] = $businessTime;
      } else {
        $viewData['businessTime'] = json_decode($data->openTime, true);
      }          
    }

		return View::make("admin/time", $viewData);
  }

  public function turnOnOrOffTheStore() {
    $viewData = array();
    $viewData['success'] = NULL;

    $data = businessTime::where('shopUserId', '=', Auth::user()->id)->first();

    if (Request::isMethod('post')) {
      // dd(Input::get('isShopTurnedOff'));
      $data->isRunning = Input::get('isShopTurnedOff') ? 1 : 0; 
      $data->save();
      $viewData['success'] = 'shop running status has been updated!';
    }

    if (empty($data)) {
      $viewData['isRunning'] = false;
    } else {
      $viewData['isRunning'] = $data->isRunning;
    }
    
    // var_dump($data);

    return View::make("admin/turnOnOrOffStore", $viewData);
  }
  /**
	* functon to create products table
	**/
	protected function installTimeTable(){
		if (Schema::hasTable("business_time")) {
			echo "businessTime table already exist!<br>";
			return false;
		}

		echo "create businessTime table ...<br>";
		Schema::create("business_time", function($table){
			$table->increments('id');
			$table->text("openTime");
			$table->integer("isRunning");
      $table->integer("shopUserId");
			$table->timestamps();
		});
	}
}
