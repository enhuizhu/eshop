<?php
   
class deliveryController extends \BaseController{
  public function __construct(){
		$this->beforeFilter("auth");
  }

  public function getDeliveryData() {
    return delivery::where('shopUserId', '=', Auth::user()->id)->first();
  }

  public function index() {
    // should check if max distance has been set up
    $data = $this->getDeliveryData();
    $viewData = array();
    $defaultDelivery = $this->getDefaultDelivery($data);
    $viewData['defaultDelivery'] = $defaultDelivery;

    if (Request::isMethod('post')) {
      $data->info = Input::get('info');
      $data->save();
    }

    if (empty($data) || empty($data->maxDistance)) {
      $viewData['info'] = 'please set the max distance first!';
    } else {
      $viewData['maxDistance'] = $data->maxDistance;
      
      if (empty($data->info)) {
        $viewData['deliveryInfo'] = [$defaultDelivery];
      } else {
        $viewData['deliveryInfo'] = json_decode($data->info, true);
      }
    }

    return View::make('admin/deliverFees', $viewData);
  }

  public function getDefaultDelivery($data) {
    if (empty($data) || empty($data->maxDistance)) {
      throw new Exception('please set the max distance first!');
      return ;
    }
    
    $delivery = new stdClass;
    $delivery->from = 0;
    $delivery->to = $data->maxDistance;
    // it need to be set up on database level 
    $delivery->fee = 1;
    $delivery->priority = 0;
    
    return $delivery;
  }

  public function setDistance() {
    $viewData = array();
    $viewData['success'] = NULL;
    $data = $this->getDeliveryData();

    if (Request::isMethod('post')) {
      if (empty($data)) {
        $data = new delivery();
        $data->shopUserId = Auth::user()->id;
      }

      $data->maxDistance = Input::get('distance');
      $data->save();
      $viewData['success'] = 'Max delivery distance has been updated successfully!';
    }

    $viewData['distance'] = empty($data) ? 0 : $data->maxDistance;
    
    return View::make('admin/deliveryDistance', $viewData);
  }

  /**
   * function to calculate the delivery fees
   */
  public function calculateDeliveryFee($distance, $info) {
    $filterInfo = [];

    foreach ($info as $v) {
      if ($distance >= $v->from && $distance <= $v->to) {
        array_push($filterInfo, $v);
      }
    }

    if (count($filterInfo) == 0) {
      return false;
    }
    
    $candidate = $filterInfo[0];
    
    foreach ($filterInfo as $v) {
      if ($v->priority > $candidate->priority) {
        $candidate = $v;
      }
    }

    return $candidate->fee;
  }

  /**
	* function to create products table
	**/
	protected function installDeliveryTable(){
		if (Schema::hasTable("delivery")) {
			echo "delivery table already exist!<br>";
			return false;
		}

		echo "create delivery table ...<br>";
    
    Schema::create('delivery', function($table) {
			$table->increments('id');
			$table->text('info');
      $table->integer('shopUserId');
      $table->integer('maxDistance');
			$table->timestamps();
		});
	}
}
