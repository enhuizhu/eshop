<?php

class OrderController extends BaseController {
	protected function installOrderTable() {
		if (Schema::hasTable('orders')) {
			echo "orders table already exist<br>";
			return false;
		}

		echo "create order table ...<br>";

		Schema::create('orders', function($table) {
			$table->increments('id');
			$table->string('deliver_time');
			$table->string('order_time');
			$table->text('items');
			$table->text('note');
			$table->char('status', 1);
			$table->char('read', 1);
			$table->integer('contact_id');
			$table->integer('user_id');
		});
	}

	protected function createAddressTable() {
		if (Schema::hasTable('contact')) {
			echo "address table already exist!<br>";
			return false;
		}

		echo "create address table ...<br>";

		Schema::create('contact', function($table) {
			$table->increments('id');
			$table->string('tel');
			$table->string('address1');
			$table->string('address2');
			$table->string('city');
			$table->string('postcode');
			$table->integer('user_id');
			$table->timestamps();
		});
	}

	public function placeOrder() {
		$orderInfo = apiService::getRawData();
		return Response::json(ordersService::saveOrders($orderInfo));
	}

	public function updateOrderStatus() {
		$orderInfo = apiService::getRawData();
		return Response::json(ordersService::updateOrderStatus($orderInfo));
	}

	public function getOrderById($id) {
		return ordersService::getOrderById($id);
	}

	public function live() {
		return View::make("admin/liveOrders");
	}
}