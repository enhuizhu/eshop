<?php

class userprofileController extends BaseController {
	public function get() {
		$user = usersService::getUserBaseOnHeaderToken();
		
		if (empty($user)) {
			return Response::json(apiService::apiErrorResponse('can not find user'));
		}

		return Response::json(array(
			'shopName' => $user->shop_name,
			'logo' => $user->logo,
			'currency' => Config::get('constants.CURRENCIES')[$user->currency]
		));
	}
}
