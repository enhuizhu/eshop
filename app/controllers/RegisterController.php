<?php

class RegisterController extends BaseController {
	/**
	* funciton to create customer table
	**/
	protected function installCustomerTable() {
		if(Schema::hasTable("customer")) {
			echo "customer table already exist<br>";
			return false;
		}

		echo "create customer table ...<br>";

		Schema::create("customer", function($table) {
			$table->increments('id');
			$table->string('username', 50);
			$table->string('password', 250);
			$table->string('email', 50);
			$table->timestamps();
		});
	}


	/**
	* function to register user
	**/
	public function createUser() {
		$userInfo = apiService::getRawData();
		return Response::json(customerService::createUser($userInfo));
	}
}