<?php

class SearchController extends BaseController {

	public function get()
	{
		$pageNumber = Request::input('page', 1);
		$products = productService::getAllProducts($pageNumber, null, Request::input('keyword', null));
		
		return Response::json($products);
	}

}
