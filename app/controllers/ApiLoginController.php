<?php

class ApiLoginController extends BaseController {
	public function login() {
		$loginInfo = apiService::getRawData();
		return Response::json(customerService::login($loginInfo));
	}

	public function shopUserLogin() {
		$loginInfo = apiService::getRawData();
		return Response::json(usersService::apiLogin($loginInfo));
	}
}
