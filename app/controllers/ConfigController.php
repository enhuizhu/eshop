<?php

class ConfigController extends BaseController {
	
	public $configs = [
		"status" => [
			0 => 'recieved order',
			1 => 'cooking',
			2 => 'delivery',
			3 => 'done',
		]
	];

	public function getConfigs() {
		return $this->configs;
	}
}