<?php

class ProductsApiController extends BaseController {
	public function getProducts($category = null) {
		$pageNumber = Request::input('page', 1);
		$products = productService::getAllProducts($pageNumber, [$category]);
		
		return Response::json($products);
	}

	public function getTokenRelatedProducts() {
		return Response::json(productService::getProductsWithCategory(Request::header('shop-token')));
	}
}