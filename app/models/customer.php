<?php
/**
* ORM for customer
**/
class customer extends Eloquent {
	protected $table = "customer";
	protected $primaryKey = 'id';

	public function orders() {
		return $this->hasMany("orders");
	}

}

