<?php

class orders extends Eloquent {
	protected $table = "orders";
	protected $primaryKey= 'id';
	public $timestamps = false;

	public function customer() {
		return $this->belongsTo('customer', 'user_id');
	}

	public function contact() {
		return $this->belongsTo('contact');
	}

}