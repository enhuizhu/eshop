<?php
/**
* ORM for category
**/
class category extends Eloquent{
	
	protected $table = "category";

	protected $primaryKey='id';

	public function products(){
		return $this->hasMany("products", "category_id");
	}
}


