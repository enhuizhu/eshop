<?php
/**
* ORM for products
**/
class products extends Eloquent{	
	
	protected $table = "products";

	protected $primaryKey='id';

	public function category(){
		return $this->belongsTo("category","category_id");
	}
}


