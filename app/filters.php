<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
    if (Auth::guest()) {
    	return Redirect::to("admin/login");
    }
});

/**
* if user already login should redirect to admin
**/
Route::filter('alreadyLogin',function(){
	if (!Auth::guest()) {
		return Redirect::to("admin");
	}
});

/**
* check if user send valid token to api
**/
Route::filter('api.login', function() {
	 if(empty(Input::get("token"))){
	 	return Response::json(apiService::apiErrorResponse("token is required"));
	 }

	 if (!apiService::isValidUserToken(Input::get("token"))) {
	 	return Response::json(apiService::apiErrorResponse("invalid token"));
	 }
});


/**
* check if user have valid desktop token
**/
Route::filter('desktop.validate', function() {
	$token = Config::get('constants.DESKTOP_TOKEN');

	if (Request::header('token') !== $token) {
	 	return Response::json(apiService::apiErrorResponse("invalid desktop token"));
	}
});


Route::filter("auth.admin", function(){

	/**
	* check if user login as admin
	**/
	$userDetails = Auth::user();

	if (Auth::check() && $userDetails->role == 2) {
	    // return "ok"; 
	}else{
		return Response::view('errors.403', array(), 403);
		// die();
	}


});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});
