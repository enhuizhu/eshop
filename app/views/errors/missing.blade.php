<!doctype html>
<html>
  <head>
     <title>error</title>
  	 <style type="text/css">
  	 	.container404{
			    top: calc(50% - 81px);
			    position: absolute;
			    width: 100%;
			    text-align: center;
			    font-size: 17px;
  	 	}

  	 	.style404{
  	 		    font-size: 800%;
			    letter-spacing: 51px;
			    text-shadow: 11px 6px 2px gray;
  	 	}
  	 </style>
  </head>
  <body>
  		<div class="container404">
  			<span class="style404">404</span>
  			<div>can not find the page!</div>
  		</div>
  </body>
</html>