@extends('admin/template')

@section('title')
	Admin Dathboard
@endsection

@section('content')
		<div class="container-fluid">
			<div class="row">

				<div class="blocks-container">
				  
				  @foreach(menuService::$menus as $k => $v)

					<?php
						$keies = array_keys($v["items"]);
						$key = isset($keies[1]) ? $keies[1] : $keies[0];
						$options = $v["items"][$key];
						 /**
			        	 * should check if it's admin or not
			        	 **/
			        	 if (!userPermission::isAdmin() && !empty($v["adminOnly"]) && $v["adminOnly"]) {
			        	 	continue;
			        	 }					
					?>
					<a href="{{$options["url"]}}" class="block btn">
						<span class="glyphicon {{$v["icon"]}}"></span><br>
						{{strtoupper($v["keyLable"])}}
					</a>

				  @endforeach

				</div>



			</div>
		</div>
@endsection