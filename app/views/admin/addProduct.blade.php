@extends('admin/template')

@section('title')
	Admin Dathboard
@endsection

@section('content')
		
		<div class="container-fluid">
			<div class="row">
				<?php
					if (isset($errors)) {
						message::errors($errors);
					}

					if (isset($success)) {
						message::success($success);
					}

					$dbData = isset($product) ? $product : null;

					$reset = isset($success) ? true : false;

				?>
				<form method="post" enctype="multipart/form-data">
					<div class="form-group">
						<label>Product Name:</label>
						<input type="text" name="name" class="form-control" value="{{commonFunctionService::getFieldDefaultValue($dbData,"name","name",$reset)}}" placeholder="product">
					</div>

					<div class="form-group">
						<label>Product discription:</label>
						<textarea name="description" class="form-control" placeholder="description">{{commonFunctionService::getFieldDefaultValue($dbData,"description","description",$reset)}}
						</textarea>
					</div>

					<div class="form-group">
						<label>Price:</label>
						<input type="text" name="price" class="form-control" value="{{commonFunctionService::getFieldDefaultValue($dbData,"price","price",$reset)}}" placeholder="£">
					</div>

					<div class="form-group radio-group">
						<label>Category:</label><br>
						@foreach($categories as $k => $v)
						  @if((Input::get("category_id") == $k && !$reset) || (isset($product) && $product["category_id"] == $k))
						 		<input type="radio" name="category_id" value="{{$k}}" checked> {{$v}}
						  @else
						 		<input type="radio" name="category_id" value="{{$k}}"> {{$v}}
						  @endif
						@endforeach
					</div>

					<div class="form-group">
						<label>Product Picture:</label>
						<input type="file" name="pic" class="form-control">
						@if(isset($product))
							<img src="{{url("uploads/".$product["pics"])}}" width="100">
						@endif
					</div>

					<div class="form-group">
						<input type="submit" value="{{isset($product) ? "Update Product" : "Create Product" }}" class="btn btn-primary">
					</div>
				</form>
			</div>
		</div>
@endsection