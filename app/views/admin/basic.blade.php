<!doctype html>
<html>
	<head>
		<title>
			@yield('title')
		</title>
		<!-- insert all the css -->
		{{HTML::style("css/sass/admin.css")}}
	</head>
	</head>
	<body>
		@yield('content')

		<!--insert all the javascripts-->
		{{HTML::script("js/jQuery.js")}}
		{{HTML::script("js/bootstrap.js")}}
	</body>
</html>