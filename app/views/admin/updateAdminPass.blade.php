@extends('admin/template')

@section('title')
	Admin Dathboard
@endsection

@section('content')
		
		<div class="container-fluid">
			<div class="row">
				
				<?php
					if (isset($errors)) {
						message::errors($errors);
					}

					if (isset($success)) {
						message::success($success);
					}
				?>
				
				<form method="post">
					<div class="form-group">
						<label>
							<span class="glyphicon glyphicon-user"></span>
							{{$user->username}}
						</label>

					</div>

					<div class="form-group">
						<label>Old Password:</label>
						<input type="password" name="oldPassword" class="form-control">
					</div>

					<div class="form-group">
						<label>New Password:</label>
						<input type="password" name="newPassword" class="form-control">
					</div>

					<div class="form-group">
						<label>Repeat New Password:</label>
						<input type="password" name="repeatNewPassword" class="form-control">
					</div>

					<div class="form-group">
						<input type="submit" class="btn btn-primary" value="Update Password">
					</div>

				</form>

			</div>
		</div>
@endsection