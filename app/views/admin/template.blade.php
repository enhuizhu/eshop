<!doctype html>
<html>
	<head>
		<title>
			@yield('title')
		</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- insert all the css -->
		{{HTML::style("css/sass/admin.css")}}
		<!--insert all the javascripts-->
		{{HTML::script("js/jQuery.js")}}
		{{HTML::script("js/bootstrap.js")}}
		<script src="https://unpkg.com/vue"></script>
	</head>
	</head>
	<body>
		 
		<div class="container-fluid">
			<div class="header row">
				<div class="site-title pull-left">Work Express</div> 
				<div class="user pull-right"> 					
					<span class="glyphicon glyphicon-user"></span>
					<div class="dropdown">
						<span id="dLabel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							{{Auth::user()->username}}
							 <span class="caret"></span>
						</span>
						<ul class="dropdown-menu" aria-labelledby="dLabel">
							<li><a href="{{url('admin/updateAdmin/'.Auth::user()->id)}}">My Profile</a></li>
							<li><a href="{{url('admin/logout')}}">Log Out</a></li>
						</ul>
					</div>
				</div>

				<div class="home-link pull-right">
					<a href="{{url("admin")}}">
						<span class="glyphicon glyphicon-home"></span>
					</a>
				</div>

				<div class="clearfix"></div>
			</div>
		</div>

		<div class="container-fluid content-container">
			<div class="row">
				<div class="side-bar pull-left">
					 {{menuService::generateMenu()}}

					 <div class="btn-more">
					 	<span class="glyphicon glyphicon-chevron-right"></span>
					 </div>
				</div>

				<div class="content-body pull-left">
					@yield('content')

<!-- 					<div class="footer">
						<span class="copy">&copy;CopyrigHt @ Online Marketing Solution</span>
					</div>
 -->				</div>
				<div class="clear"></div>
			</div>
		</div>
		{{HTML::script("js/global.js")}}
	</body>
</html>