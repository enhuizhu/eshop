@extends('admin/template')

@section('title')
	Admin Dathboard
@endsection

@section('content')
  <style type="text/css">
   .control-label {
     text-align: left !important;
   }
  </style>
  <div class="container-fluid">
    <div class="row">
      {{message::success($success)}}
      <form class="form-inline" method="post">
        <div class="checkbox">
          <label>
            <input 
              type="checkbox" 
              name="isShopTurnedOff"
              {{$isRunning ? 'checked' : ''}}
            > Turn off the online shop
          </label>
        </div>
        <!-- <div class="form-group">
          <label class="col-sm-3 control-label">
            Turn off the online shop
          </label>
          <div class="col-sm-9">
            <input 
              class="form-control" 
              type="checkbox" 
              name="isShopTurnedOff"
              {{$isRunning ? 'checked' : ''}}
            >
          </div>        
        </div> -->
        <input type="submit" value="Update" class="btn btn-primary pull-right"/>

        <div class="form-group">
        </div>
      </form>
    </div>
  </div>
@endsection