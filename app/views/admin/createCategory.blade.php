@extends('admin/template')

@section('title')
	Admin Dathboard
@endsection

@section('content')
		
		<div class="container-fluid">
			<div class="row">
				<?php
					if (isset($errors)) {
						message::errors($errors);
					}

					if (isset($success)) {
						message::success($success);
					}
				?>

				<form method="post" enctype="multipart/form-data">
				   <div class="form-group radio-group">
				   		<label>
				   			Please select your parent category:
				   		</label><br>
				   		@foreach($categories as $id => $name)
				   		   @if(Input::get("parent_id") == $id || (isset($category) && $category->parent_id == $id))
					   		   <input type="radio" value="{{$id}}" name="parent_id" checked> {{$name}}
				   		   @else
					   		   <input type="radio" value="{{$id}}" name="parent_id"> {{$name}}
				   		   @endif
				   		@endforeach
				   </div>
				   <div class="form-group">
				   		<label>
				   			Name	
				   		</label>
				   		<input type="text" name="name" class="form-control" value="{{isset($category) ? $category->name : Input::get("name")}}">
				   </div>
				   <div class="form-group">
				   		<label>
				   			Category image
				   		</label>
				   		<input type="file" name="pic" class="form-control">

				   		@if(isset($category))
				   			<img src="{{url("uploads/".$category->pic)}}" width="140" class="margin-top-10">
				   		@endif
				   </div>
				   <div class="form-group">
				   		<input type="submit" value="{{isset($category) ? "Update Category" : "Create Category" }}" class="btn btn-primary">
				   </div>

				</form>
			</div>
		</div>
@endsection