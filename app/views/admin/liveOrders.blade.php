@extends('admin/template')

@section('title')
	Live Orders
@endsection

@section('content')
		
		<div class="container-fluid">
			<div class="row">
				<iframe src="/cms/orders/index.html"></iframe>
			</div>		
		</div>

		<script type="text/javascript">
			function setIframeHeight() {
				jQuery("iframe").height(jQuery(window).height());
			}

			jQuery(document).ready(function() {
				setIframeHeight();

				jQuery(window).resize(setIframeHeight);
			});
		</script>
@endsection