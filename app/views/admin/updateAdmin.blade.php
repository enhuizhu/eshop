@extends('admin/template')

@section('title')
	Admin Dathboard
@endsection

@section('content')
		
		<div class="container-fluid">
			<div class="row">
				
				<?php
				 if(isset($message)):
					message::info($message);
				 endif
				?>
				
				<table class="table table-border table-striped">
					<tr>
						<td>User Name</td>
						<td>Email</td>
						<td>Status</td>
						<td>Role</td>
						@if(Auth::user()->role == 2)
							<td>Edit</td>
							<td>Delete</td>
							<td>Change Password</td>
						@endif
					</tr>
					@foreach($users as $user)
					  <tr>
					  	<td>{{$user["username"]}}</td>
					  	<td>{{$user["email"]}}</td>
					  	<td>{{$user["active"] == 1 ? "Active" : "Inactive"}}</td>
					  	<td>{{$user["role"] == 1 ? "User" : "Administrator"}}</td>
						@if(Auth::user()->role == 2)
							<td><a href="{{url("admin/updateAdmin/".$user["id"])}}" class="btn btn-primary">Edit</a></td>
							<td><a href="{{url("admin/deleteAdmin/".$user["id"])}}" class="btn btn-danger">Delete</td>
							<td><a href="{{url("admin/updateAdminPass/".$user["id"])}}" class="btn btn-danger">Change Password</td>
						@endif

					  </tr>
					@endforeach
				</table>
			</div>
		</div>
@endsection