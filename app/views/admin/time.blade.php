@extends('admin/template')

@section('title')
	Admin Dathboard
@endsection

@section('content')
    <style type="text/css">
      .form-control {
        width: 80%;
        display: inline-block;
      }

      .btn.btn-primary {
        margin-right: 20px;
      }
    </style>
		<div class="container-fluid">
			<div class="row">
        {{message::success($success)}}
        <form class="form-horizontal" method="post">
          @foreach($businessTime as $k => $v)
            <div class="form-group">
              <label class="col-sm-2 control-label">{{$k}}</label>
              <div class="col-sm-10">
                <div class="form-group">
                  <label class="col-sm-2 control-label">from</label>
                  <input type="time" 
                    class="form-control" 
                    placeholder="start time"
                    name="{{$k}}[start]"
                    required 
                    value="{{$v['start']}}">
                </div>
               
                <div class="form-group">
                  <label class="col-sm-2 control-label">to</label>
                  <input type="time" 
                    class="form-control" 
                    placeholder="end time"
                    name="{{$k}}[end]"
                    required 
                    value="{{$v['end']}}">
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">Closed</label>
                  <input type="checkbox"
                    class="form-control" 
                    name="{{$k}}[close]"
                    {{isset($v["close"]) && $v["close"] ? "checked" : ""}}
                  >
                </div>
              </div>
            </div>
          @endforeach
            <div class="form-group">
              <input type="submit" class="btn btn-primary pull-right" value="Set Time"/>
              <div class="clearfix"></div>
            </div>
        </form>
			</div>
		</div>
@endsection