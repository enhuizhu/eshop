@extends('admin/template')

@section('title')
	Admin Dathboard
@endsection

@section('content')
		
		<div class="container-fluid">
			<div class="row max-width-400">
					<?php
						if (isset($errors)) {
							message::errors($errors);
						}

						if (isset($success)) {
							message::success($success);
						}
					?>



					<form method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>User Name:</label>
							<input type="text" name="username" class="form-control" value="{{isset($user)? $user->username : Input::get("username")}}">
						</div>

						<div class="form-group">
							<label>Shop Name:</label>
							<input type="text" name="shop_name" class="form-control" value="{{isset($user)? $user->shop_name : Input::get("shop_name")}}">
						</div>

						<div class="form-group">
							<label>Shop Logo:</label>
							<input type="file" name="logo" class="form-control">
							@if(isset($user) && !empty($user["logo"]))
								<img src="{{url("uploads/".$user["logo"])}}" width="100">
							@endif
						</div>

						@if(!isset($mode) || $mode!="update")

						<div class="form-group">
							<label>Password</label>
							<input type="password" name="password" class="form-control">
						</div>

						<div class="form-group">
							<label>Repeat Password</label>
							<input type="password" name="re-password" class="form-control">
						</div>

						@endif


						<div class="form-group">
							<label>Email</label>
							<input type="text" name="email" class="form-control" value="{{isset($user)? $user->email : Input::get("email")}}">
						</div>

						<div class="form-group">
							<label>Roles</label>
							<select name="roles" class="form-control">
								@foreach($roles as $k => $v)
								   <?php
								   	   if (isset($user) && $user->role == $v || Input::get("roles") == $v) {
								   	   	   $selected = " selected";
								   	   }else{
								   	   	   $selected = "";
								   	   }
								   ?>
								   <option value="{{$v}}"{{$selected}}>{{$k}}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group">
							<label>Status</label>
							<select name="status" class="form-control">
								@foreach($status as $k => $v)
   								   <?php
								   	   if (isset($user) && $user->active == $v || Input::get("status") == $v) {
								   	   	   $selected = " selected";
								   	   }else{
								   	   	   $selected = "";
								   	   }
								   ?>
								   <option value="{{$v}}"{{$selected}}>{{$k}}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group">
							<label>Currencies</label>
							<select name="currency">
								@if(!isset($user) || isset($user) && empty($user->currency))
								   <option value="">please currency</option>
								@endif
								
								@foreach($currencies as $k => $v)
   								   <?php
								   	   if (isset($user) && $user->currency == $k || Input::get("currency") == $k) {
								   	   	   $selected = " selected";
								   	   }else{
								   	   	   $selected = "";
								   	   }
								   ?>
								   <option value="{{$k}}"{{$selected}}>{{$k}}</option>
								@endforeach
							</select>
						</div>

						@if(isset($mode) && $mode == "update")
						<div class="form-group">
							<label>Token</label>
							<input type="text" name="token" class="form-control" value="{{$user->token}}" readonly>
						</div>
						@endif

						<div class="form-group">
							<input type="submit" name="Create New User" class="btn btn-primary pull-right"> 
						</div>
					</form>
			</div>
		</div>
@endsection