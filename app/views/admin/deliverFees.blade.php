@extends('admin/template')

@section('title')
  Dlivery Fees
@endsection

@section('content')
  <div class="container-fluid" id="app">
    <div class="row">
      @if(isset($info))
        {{message::info($info)}}
      @else
        <form method="post" ref="form">
          <div v-if="info">
            <delivery-fee  
              v-bind:from="item.from"
              v-bind:to="item.to"
              v-bind:fee="item.fee"
              v-bind:priority="item.priority"
              v-bind:max-distance="maxDistance"
              v-bind:show-delete="index!==0"
              v-bind:id="index"
              v-on:update-callback="onUpdate($event)"
              v-for="(item, index) in info"
              @del="deleteItem"
              :key="index"
              ref="d"
            >
            </delivery-fee>
            <input type="hidden" name="info" ref="info"/>
          </div>

          <div class="form-group">
            <div 
              type="submit" 
              class="btn btn-primary pull-right"
              v-on:click="onUpdate()"
            >Update</div>
            <div 
              type="submit" 
              class="btn btn-info pull-right margin-right-20"
              v-on:click="addNew()"
            >Add New</div> &nbsp;&nbsp;
          </div>
        </form>
      @endif
    </div>
  </div>
  <style type="text/css">
    .delivery-fee {
      margin-bottom: 10px;
    }
    .delivery-fee input:not([type=range]) {
      width: 40px;
    }

    .delivery-fee input[type=range] {
      display: inline-block;
      width: auto;
    }
  </style>
  <script language="javaScript">
    Vue.component('delivery-fee', {
      template: `
        <div class="delivery-fee">
          <div class="alert alert-danger" role="alert" v-if="error">
            @{{errorMessage}}
          </div>
          <span>from</span>
          <input
            type="number" 
            v-bind:value="from"
            v-on:change="validate()"
            ref="from"
          />
          miles
          <span>&nbsp; to &nbsp;</span>
          <input 
            type="number" 
            v-bind:value="to"
            v-on:change="validate()"
            ref="to" 
          />
          miles &nbsp;&nbsp;
          <span>Fees</span>
          &pound;<input
            type="number"
            v-bind:value="fee"
            ref="fee"
          />
          &nbsp;&nbsp;
          priority
          <input 
            type="range"
            ref="priority"
            min="0"
            max="10"
            size="100"
            v-bind:value="priority" 
          >
          <div 
            class="btn btn-danger btn-sm" 
            v-if="showDelete"
            v-on:click="del()"
          >Delete</div>
          <div class="clear"></div>
        </div>
      `,
      props: [
        'from', 
        'to', 
        'fee',
        'maxDistance', 
        'showDelete',
        'priority',
        'id'
      ],
      data: function() {
        return {
          error: false,
          errorMessage: '',
        }
      },
      created: function() {
        console.log('vue hook created', this.error, this.maxDistance);
        if (this.from > this.maxDistance
          || this.to > this.maxDistance) {
          this.setErrorMsg(`the value of from or to can not bigger then 6 miles`);
        }
      },
      methods: {
        update: function() {
          this.$emit('update-callback', {
            from: this.$refs['from'].value,
            to: this.$refs['to'].value,
            fee: this.$refs['fee'].value
          });
        },

        setErrorMsg: function(msg) {
          this.error = true;
          this.errorMessage = msg;
        },

        validate: function() {
          console.log('validating');
          const from = this.$refs['from'].value;
          const to = this.$refs['to'].value;
          
          if (from >= to) {
            this.setErrorMsg(`from can not be greater then to.`);
          } else if (from >= this.maxDistance
            || to > this.maxDistance) {
            this.setErrorMsg(`the value of from or to can not be greater then ${this.maxDistance} miles.`);
          } else {
            this.error = false;
          }
        },

        del: function() {
          console.log('id', this.id);
          this.$emit('del', this.id);
        },

        getValue: function() {
          return {
            from: parseInt(this.$refs['from'].value),
            to: parseInt(this.$refs['to'].value),
            fee: parseInt(this.$refs['fee'].value),
            priority: parseInt(this.$refs['priority'].value)
          }
        }
      },
    });
    
    var app = new Vue({
      el: '#app',
      data: {
        maxDistance: {{$maxDistance}},
        info: {{empty($deliveryInfo) ? 'null' : json_encode($deliveryInfo)}},
        defaultFee: 1,
        defaultDelivery: {{json_encode($defaultDelivery)}}
      },
      methods: {
        onUpdate: function() {
          const deliveryData = this.$refs['d'].map(v => v.getValue());
          this.$refs['info'].value = JSON.stringify(deliveryData);
          this.$refs['form'].submit();
        },
        
        addNew: function() {
          console.log('defaultDelivery', this.defaultDelivery);
          console.log('info', this.info);
          this.info.push(this.defaultDelivery);
        },

        deleteItem: function(itemIndex) {
          console.log('itemIndex', itemIndex);
          this.info.splice(itemIndex, 1);
        },

      }
    })
  </script>
@endsection