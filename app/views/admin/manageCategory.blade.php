@extends('admin/template')

@section('title')
	Admin Dathboard
@endsection

@section('content')
		
		<div class="container-fluid">
			<div class="row">
				<?php
					if (isset($errors)) {
						message::errors($errors);
					}

					if (isset($success)) {
						message::success($success);
					}

					if (isset($message)) {
						message::info($message);
					}
				?>

				<table class="table table-border table-striped">
					
					<tr>
						<th>Id</th>
						<th>Name</th>
						<th>Picture</th>
						<th>Parent Id</th>
						<th>Update</th>
						<th>Delete</th>
					</tr>

					@foreach($categories as $category)
					<tr>
						<td>{{$category["id"]}}</td>
						<td>{{$category["name"]}}</td>
						<td><img src="{{url("uploads/".$category["pic"])}}" width="100"></td>
						<td>{{$category["parent_id"] == 0 ? "Root" : $categories[$category["parent_id"]]["name"]}}</td>
						<td> <a href="{{url("admin/category/update/".$category["id"])}}" class="btn btn-primary">Update</a> </td>
						<td> <a href="{{url("admin/category/delete/".$category["id"])}}" class="btn btn-danger delete">Delete</a> </td>
					</tr>
					@endforeach


				</table>

				<script type="text/javascript">
						jQuery(document).ready(function(){
							jQuery("a.delete").bind("click", function(e){
								e.preventDefault();
								
								var isGoingToDelete = confirm("It will delete all the products and sub categories!");
								
								if (isGoingToDelete) {
								   var url = jQuery(this).attr("href");
								   location.href = url;
								};
							});
						});
				</script>

			</div>
		</div>
@endsection