@extends('admin/basic')

@section('title')
	Admin Login
@stop

@section('content')
	<div class="container login">
	     @if (Session::has("login_errors"))
		     <div class="alert alert-danger alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			  	<span aria-hidden="true">&times;</span>
			  </button>
			     Incorrect username or password!
			 </div>
		 @endif

	    <div class="content">
	      <div class="row">
	        <div class="login-form">
	          <h2>Login</h2>

	          {{Form::open()}}
	            <fieldset>
	              <div class="clearfix">
	               {{Form::text("username",Input::old('username'),  array('placeholder'=>'Username', 'class'=>'form-control'))}}
	              </div>
	              <div class="clearfix">
	                {{Form::password("password", array('placeholder'=>'Password', 'class'=>"form-control"))}}
	              </div>
	              {{Form::submit("Login", array("class"=>"btn btn-primary"))}}
	              <!-- <button class="btn btn-primary" type="submit">Sign in</button> -->
	            </fieldset>
	          {{Form::close()}}
	        </div>
	      </div>
	    </div>
  </div> <!-- /container -->
@stop