@extends('admin/template')

@section('title')
	Admin Dathboard
@endsection

@section('content')
		
		<div class="container-fluid">
			<div class="row">
				<?php
					if (isset($errors)) {
						message::errors($errors);
					}

					if (isset($success)) {
						message::success($success);
					}

					if (isset($msg)) {
						message::info($msg);
					}
				?>

				
				<?php
				    $categoriesNoRoot = categoryService::getCategoryNoRoot(true);
				?>

				<form class="form-inline" method="post">
					
					<div class="form-group">
						<label>Category</label>
						<select class="form-control" name="filterCategory">
							<option value="0">All</option>
							@foreach($categoriesNoRoot as $id => $name)
								<option value="{{$id}}"{{Session::get("category_id") == $id ? " selected" : ""  }}>{{$name}}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<label>Key Word</label>
						<input type="text" name="keyWord" placeholder="key word" class="form-control" value='{{Session::get("name")}}'>
					</div>

					<div class="form-group">
						<input type="submit" value="Search" class="btn btn-primary">
					</div>


				</form>

				<p>&nbsp;</p>
				
				<table class="table table-border table-striped">
					  <tr>
					  	  <th>Product Name</th>
					  	  <th>Product Description</th>
					  	  <th>Price</th>
					  	  <th>Image</th>
					  	  <th>Category</th>
					  	  <th>Update</th>
					  	  <th>Delete</th>
					  </tr>
					  <?php
					  	$categoriesWithIdAsKey = categoryService::getCategoriesWithIdAsKey();
					  	$categoryNames = array();
					  ?>

					  @foreach($products as $product)
					  	<tr>
					  	 	<td>{{$product["name"]}}</td>  
					  	 	<td>{{$product["description"]}}</td>  
					  	 	<td>{{productService::generateCurrency($product["price"])}}</td>  
					  	 	<td> <img src="{{url("uploads/".$product["pics"])}}" width="100"></td>  
					  	 	<td>
					  	 		<?php
					  	 		   if (!isset($categoryNames[$product["category_id"]])) {
					  	 		   	   $currentCategory = category::find($product["category_id"]);
					  	 		   	   $categoryNames[$product["category_id"]] = categoryService::getMenuNameWithParentName($categoriesWithIdAsKey,$currentCategory); 
					  	 		   }

					  	 		   echo $categoryNames[$product["category_id"]];
					  	 		?>
					  	 	</td>
					  	 	<td>
					  	 		<a href="{{url("admin/products/updateProduct/".$product["id"])}}" class="btn btn-primary">Update</a>
					  	 	</td>
					  	 	<td>
					  	 		<a href="{{url("admin/products/deleteProduct/".$product["id"])}}" class="btn btn-danger">Delete</a>
					  	 	</td>
					  	</tr>
					  @endforeach
				</table>

				<?php
					echo commonFunctionService::paginations($totalPages,$pageNumber,'admin/products/manageProducts');
				?>

			</div>
		</div>
@endsection