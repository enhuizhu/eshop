@extends('admin/template')

@section('title')
	Dlivery Distance
@endsection

@section('content')
  <div class="container-fluid">
    <div class="row">
      {{message::success($success)}}
      <form method="post" class="form-inline">
        <div class="form-group">
          Max Delivery Distance:
          <input 
            type="number" 
            name="distance" 
            class="form-control" 
            value="{{$distance}}"
            required
          > miles
        </div>
        <input 
          type="submit" 
          value="Update"
          class="btn btn-primary pull-right"
        />
      </form>
    </div>
  </div>
@endsection