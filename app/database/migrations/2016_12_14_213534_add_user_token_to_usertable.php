<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserTokenToUsertable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
			//check if token field already in the table
			if (!Schema::hasColumn('users', 'token')) {
				$table->char('token', 40);
			}
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
			if (Schema::hasColumn('users', 'token')) {
				$table->dropColumn('token');
			}
		});
	}
}
