<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShopnameToUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
			//check if the shop_name already exist
			if (!Schema::hasColumn('users', 'shop_name')) {
				$table->char('shop_name', '200');	
			}
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
			if (Schema::hasColumn('users', 'shop_name')) {
				$table->dropColumn('shop_name');
			}
		});
	}

}
