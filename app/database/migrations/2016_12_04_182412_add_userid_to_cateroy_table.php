<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUseridToCateroyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('category', function(Blueprint $table)
		{
			//should check if the column of userid already exist
			if (!Schema::hasColumn('category','user_id')) {
				$table->unsignedInteger('user_id');
			}
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('category', function(Blueprint $table)
		{
			//delete the column
			if (Schema::hasColumn('category', 'user_id')) {
				$table->dropColumn('user_id');
			}
		});
	}

}
