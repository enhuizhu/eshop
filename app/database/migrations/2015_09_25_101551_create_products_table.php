<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	   //create user table
	   if (!Schema::hasTable("products")) {
		   Schema::create("products", function($table){
			   	$table->increments('id');
				$table->string("name", 255);
				$table->text("description");
				$table->float("price");
				$table->integer("category_id");
				$table->text("pics");
				$table->string("feature_pic", 255);
				$table->timestamps();
		   });
	   }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("products");
	}

}
