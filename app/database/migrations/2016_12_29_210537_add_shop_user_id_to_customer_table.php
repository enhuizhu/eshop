<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShopUserIdToCustomerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//should check if the column already exist
		Schema::table('customer', function(Blueprint $table)
		{
			if (!Schema::hasColumn('customer', 'shop_user_id')) {
				$table->unsignedInteger('shop_user_id');
			}
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasColumn('customer', 'shop_user_id')) {
			$table->dropColumn('shop_user_id');
		}
	}
}
