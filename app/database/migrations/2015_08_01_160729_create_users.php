<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
           //create user table
	   if (!Schema::hasTable('users')) {
	   	 	Schema::create("users", function($table){
			   	$table->increments('id');
				$table->string("username", 32);
				$table->string("email", 255);
				$table->string("password",255);
				$table->integer("role");
				$table->boolean("active");
				$table->timestamps();
		   	});
	   }
	  
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	  Schema::drop("users");
	}

}
