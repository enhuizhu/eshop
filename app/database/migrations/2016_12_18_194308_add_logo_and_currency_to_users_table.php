<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLogoAndCurrencyToUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
			if (!Schema::hasColumn('users', 'logo')) {
				$table->char('logo', 200);
			}

			if (!Schema::hasColumn('users', 'currency')) {
				$table->char('currency', 100);
			}
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
			if (Schema::hasColumn('users', 'logo')) {
				$table->dropColumn('logo');
			}
	
			if (Schema::hasColumn('users', 'currency')) {
				$table->dropColumn('currency');
			}
		});
	}

}
