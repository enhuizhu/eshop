<?php
Route::get("/", function() { return 'api';});
Route::get("/userprofile", "userprofileController@get");
Route::get("/search", "SearchController@get");
Route::get("/categories", "CategoryController@get");
Route::get("/products", "ProductsApiController@getProducts");
Route::get("/products-with-token", "ProductsApiController@getTokenRelatedProducts");
Route::get("/products/{category}", "ProductsApiController@getProducts");
Route::post("/customer/register", "RegisterController@createUser");
Route::post("/customer/login", "ApiLoginController@login");
Route::post("/customer/placeOrder", array("before" => "api.login", "uses" => "OrderController@placeOrder"));
Route::get("/orders/getOrderById/{id}", "OrderController@getOrderById");
Route::get("/getConfigs", "ConfigController@getConfigs");
Route::post("/updateOrderStatus", array("before" => "desktop.validate", "uses" => "OrderController@updateOrderStatus"));
Route::post("/shopuser/login", "ApiLoginController@shopUserLogin");
Route::get("/business/open-hours", "BusinessTimeController@getBusinessHours");
