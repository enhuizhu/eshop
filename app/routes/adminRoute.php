<?php

Route::get("admin/","AdminController@dashBoard");
Route::match(array("post","get"), "admin/login", "loginController@login");
Route::get("admin/logout", "loginController@logout");
// Route::match(array("post","get"),"admin/createAdmin", "AdminController@createAdmin");
Route::match(array("post","get"),  "admin/createAdmin", array("before"=>"auth.admin","uses"=>"AdminController@createAdmin"));
Route::match(array("post","get"),"admin/manageAdmin", array("before" => "auth.admin","uses"=>"AdminController@manageAdmin"));
Route::match(array("post","get"),"admin/updateAdmin/{id}", "AdminController@updateAdmin");
Route::match(array("post","get"),"admin/updateAdminPass/{id}", "AdminController@updateAdminPass");
Route::get("admin/deleteAdmin/{id}", "AdminController@deleteAdmin");
Route::match(array("post","get"),"admin/products/createCategory", "productsController@createCategory");
Route::get("admin/products/manageCategory", "productsController@manageCategory");
Route::match(array("post","get"), "admin/category/update/{id}", "productsController@updateCategory");
Route::get("admin/category/delete/{id}", "productsController@deleteCategory");
Route::match(array("post", "get"), "admin/products/createProduct", "productsController@addProduct");
Route::any("admin/products/manageProducts/{pageNumber?}", "productsController@manageProducts");
Route::any("admin/products/updateProduct/{id}", "productsController@updateProduct");
Route::get("admin/products/deleteProduct/{id}", "productsController@deleteProduct");
Route::any("admin/liveOrder", "OrderController@live");
Route::any("admin/time", "timeController@index");
Route::any("admin/turn-on-or-off-store", "timeController@turnOnOrOffTheStore");
Route::any("admin/delivery", "deliveryController@index");
Route::any("admin/set-delivery-distance", "deliveryController@setDistance");