
<?php
/**
* route for database manipulation
**/
Route::get("/admin/product/install", "productsController@install");
Route::get("/admin/customer/install", "RegisterController@installCustomerTable");
Route::get("/admin/orders/install", "OrderController@installOrderTable");
Route::get("/admin/contact/install", "OrderController@createAddressTable");