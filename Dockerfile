FROM webdevops/php-nginx:ubuntu-15.10

# RUN apk add php-dev libmcrypt-dev php-pear
# RUN apk add mysqli pdo pdo_mysql
# RUN pecl install mcrypt-1.0.1
# RUN echo extension=/usr/lib/php/7.2/modules/mcrypt.so >> /etc/php/7.2/php.ini
# RUN echo extension=pdo.so > /etc/php/7.2/php.ini
# RUN echo extension=pdo_sqlite >> /etc/php/7.2/php.ini
# RUN echo extension=pdo_mysql >> /etc/php/7.2/php.ini
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php -r "if (hash_file('SHA384', 'composer-setup.php') === '93b54496392c062774670ac18b134c3b3a95e5a5e5c8f1a9f115f203b75bf9a129d5daa8ba6a13e2cc8a1da0806388a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"
RUN mv composer.phar /usr/local/bin/composer
ADD ./eshop.conf /etc/nginx/conf.d/
#  /var/www/myweb/
# RUN cd /var/www/myweb/ && composer install
EXPOSE 80 8080
# ;extension=pdo_mysql
# ;extension=pdo_oci
# ;extension=pdo_odbc
# ;extension=pdo_pgsql
# ;extension=pdo_sqlite